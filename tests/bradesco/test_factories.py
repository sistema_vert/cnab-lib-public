from cnab import CNAB, Batch, BatchHeader, BatchTrailer, CnabFactory
from cnab.cnab import Bankslip, FileHeader, FileTrailer, SegmentB, SegmentJ52, Transfer


def test_factory_cnab_verion_04():
    cnab = CnabFactory.get_cnab('bradesco', '240', '04')
    assert isinstance(cnab, CNAB)
    assert isinstance(cnab.header, FileHeader)
    assert isinstance(cnab.trailer, FileTrailer)


def test_factory_bacth_verion_04():
    batch = CnabFactory.get_batch('bradesco', '240', '04')
    assert isinstance(batch, Batch)
    assert isinstance(batch.header, BatchHeader)
    assert isinstance(batch.trailer, BatchTrailer)


def test_factory_transfer_verion_04():
    transfer = CnabFactory.get_transfer('bradesco', '240', '04')
    assert isinstance(transfer, Transfer)


def test_factory_transfer_detail_verion_04():
    detail = CnabFactory.get_detail('bradesco', '240', '04')
    assert isinstance(detail, SegmentB)


def test_factory_bankslip_verion_04():
    bankslip = CnabFactory.get_bankslip('bradesco', '240', '04')
    assert isinstance(bankslip, Bankslip)


def test_factory_bankslip_detail_verion_04():
    detail = CnabFactory.get_bankslip_detail('bradesco', '240', '04')
    assert isinstance(detail, SegmentJ52)
