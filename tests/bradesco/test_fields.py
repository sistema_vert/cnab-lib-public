
def test_file_header_fields(cnab_bradesco):
    header = cnab_bradesco.header
    expected_fields = ['bank_code', 'batch_number', 'registry_type', 'febraban_usage', 'company_document_type', 'cnpj',
                       'agreegment_number', 'agency', 'agency_digit', 'account_number', 'account_digit',
                       'digit_ag_account', 'company_name', 'bank_name', 'febraban_usage_cnab', 'shipping_return_code',
                       'file_generation_date', 'hour_generation_date', 'file_sequencial_number', 'layout_number',
                       'density', 'bank_reserved', 'company_reserved', 'febraban_cnab_reserved']
    assert header.fields == expected_fields


def test_file_trailer_fields(cnab_bradesco):
    trailer = cnab_bradesco.trailer
    expected_fields = ['bank_code', 'batch', 'record_type', 'febraban_usage', 'batch_qty', 'file_records_qty',
                       'accounts_qty', 'febraban_usage_cnab']
    assert trailer.fields == expected_fields


def test_batch_header_fields(batch_bradesco):
    header = batch_bradesco.header
    expected_fields = ['bank_code', 'batch_number', 'record_type', 'operation_type', 'service_type', 'launch_form',
                       'batch_layout', 'febraban_usage', 'company_document_type', 'cnpj', 'agreegment_number', 'agency',
                       'agency_digit', 'account_number', 'account_digit', 'digit_ag_account', 'company_name', 'message',
                       'street', 'street_number', 'complement', 'city', 'zipcode', 'zipcode_complement', 'state',
                       'payment_service', 'febraban_usage_cnab', 'ocurrency_code']
    assert header.fields == expected_fields


def test_batch_trailer_fields(batch_bradesco):
    trailer = batch_bradesco.trailer
    expected_fields = ['bank_code', 'batch', 'record_type', 'febraban_usage', 'batch_qty', 'amount_sum',
                       'currency_qty_sum', 'debit_notice', 'febraban_usage_cnab', 'ocurrency_code']
    assert trailer.fields == expected_fields


def test_transfer_fields(transfer_bradesco):
    expected_fields = ['bank_code', 'batch_number', 'record_type', 'record_number', 'segment_code', 'movement_type',
                       'instrution_code', 'chamber_code', 'grantee_bank_code', 'grantee_agency', 'grantee_agency_digit',
                       'grantee_account_number', 'grantee_account_digit', 'grantee_digit_ag_account', 'grantee_name',
                       'payment_id', 'payment_date', 'currency', 'currency_qty', 'amount', 'bank_payment_id',
                       'payment_real_date', 'real_amount', 'extra_informations', 'doc_purpose_code', 'ted_purpose_code',
                       'complement_purpose_code', 'febraban_usage_cnab', 'grantee_notice', 'ocurrency_code']
    assert transfer_bradesco.fields == expected_fields


def test_transfer_detail_bradesco(transfer_detail_bradesco):
    expected_fields = ['bank_code', 'batch_number', 'record_type', 'record_number', 'segment_code', 'febraban_usage',
                       'company_document_type', 'cnpj', 'street', 'number', 'complement', 'neighborhood', 'city',
                       'zipcode', 'zipcode_complement', 'state', 'due_date', 'amount', 'rebate_amount', 'discount',
                       'mora_amount', 'fine_amount', 'document_id', 'grantee_notice', 'siape', 'ispb_code']
    assert transfer_detail_bradesco.fields == expected_fields


def test_bankslip_bradesco(bankslip_bradesco):
    expected_fields = ['bank_code', 'batch_number', 'record_type', 'record_number', 'segment_code', 'movement_type',
                       'instruction_code', 'barcode', 'assignor_name', 'due_date', 'title_amount', 'discount_amount',
                       'late_payment', 'payment_date', 'amount', 'currency_qty', 'payment_id', 'payment_bank_id',
                       'currency_code', 'febraban_usage_cnab', 'ocurrency_code']

    assert bankslip_bradesco.fields == expected_fields
