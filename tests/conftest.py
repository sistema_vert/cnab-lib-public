import pytest

from cnab import CnabFactory


@pytest.fixture
def cnab_bradesco():
    """
    Generate a bradesco cnab version 04.
    with file header and file trailer.
    """
    return CnabFactory.get_cnab('bradesco', '240', '04')


@pytest.fixture
def batch_bradesco():
    """
    Generate a bradesco batch version 04.
    with batch header and batch trailer.
    """
    return CnabFactory.get_batch('bradesco', '240', '04')


@pytest.fixture
def transfer_bradesco():
    """
    Generate a bradesco transfer version 04.
    TED and DOC
    """
    return CnabFactory.get_transfer('bradesco', '240', '04')


@pytest.fixture
def transfer_detail_bradesco():
    """
    Generate a bradesco grantee detail version 04.
    """
    return CnabFactory.get_detail('bradesco', '240', '04')


@pytest.fixture
def bankslip_bradesco():
    """
    Generate a bradesco bankslip version 04.
    """
    return CnabFactory.get_bankslip('bradesco', '240', '04')
