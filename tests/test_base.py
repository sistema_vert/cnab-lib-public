from decimal import Decimal

import pytest

from cnab import CnabFactory
from cnab.base import ALPHANUMERIC, NUMERIC, Field
from cnab.exceptions import RequiredFieldError


@pytest.fixture
def field_params():
    params = {
        'name': 'test field',
        'code': '01.0',
        'size': 3,
        'description': 'test field',
        'position': (1, 3),
        'formatter': NUMERIC,
        'required': False,
    }
    return params


@pytest.mark.parametrize('propertie', ['name', 'code', 'size', 'description', 'position', 'formatter', 'required'])
def test_field_properties(propertie, field_params):
    field = Field(**field_params)
    assert hasattr(field, propertie)


def test_invalid_formatter(field_params):
    field_params['formatter'] = 'J'
    with pytest.raises(ValueError):
        Field(**field_params)


@pytest.mark.parametrize('value', [1, '1'])
def test_set_value_to_field(value, field_params):
    field = Field(**field_params)
    field.value = value
    assert field.value == '1'


def test_field_decimal_places(field_params):
    field_params['decimal_places'] = 2
    field = Field(**field_params)

    with pytest.raises(ValueError):
        field.value = '2'


def test_field_decimal_places_decimal_value(field_params):
    field_params['size'] = 5
    field_params['decimal_places'] = 2
    field = Field(**field_params)
    field.value = Decimal('2.00')

    assert field.value == '2.00'


def test_field_invalid_size(field_params):
    field_params['size'] = 2
    field = Field(**field_params)

    with pytest.raises(ValueError):
        field.value = '223'


def test_serialize_field_required_error(field_params):
    field_params['required'] = True
    field = Field(**field_params)
    field.value = ''

    with pytest.raises(RequiredFieldError):
        field.serialize()


def test_serialize_field_invalid_numeric(field_params):
    field_params['required'] = True
    field_params['formatter'] = NUMERIC
    field = Field(**field_params)
    field.value = 'A'

    with pytest.raises(ValueError):
        field.serialize()


def test_serialize_numeric_field(field_params):
    field_params['required'] = True
    field_params['formatter'] = NUMERIC
    field_params['size'] = 3

    field = Field(**field_params)
    field.value = '1'

    # complete with zeros lefth
    assert field.serialize() == '001'


def test_serialize_alphanumeric_field(field_params):
    field_params['required'] = True
    field_params['formatter'] = ALPHANUMERIC
    field_params['size'] = 3

    field = Field(**field_params)
    field.value = 'J'

    # complete with spaces right
    assert field.serialize() == 'J  '


def test_serialize_numeric_decimal_places(field_params):
    field_params['required'] = False
    field_params['size'] = 10
    field_params['decimal_places'] = 5
    field_params['formatter'] = NUMERIC

    field = Field(**field_params)
    field.value = Decimal('1234.32')

    # separe before . and after .
    assert field.serialize() == '000000123400032'


def test_add_detail_bankslip():
    bankslip = CnabFactory.get_bankslip('bradesco', '240', '04')
    detail = CnabFactory.get_bankslip_detail('bradesco', '240', '04')
    bankslip.add_detail(detail)

    assert bankslip.detail == detail
