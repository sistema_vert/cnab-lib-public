from io import StringIO

import pytest


@pytest.fixture
def buffer():
    return StringIO()


def test_export_header_cnab(cnab_bradesco, buffer):
    data = {
        'cnpj': '05422962000152',
        'agreegment_number': '9898',
        'agency': '2332',
        'agency_digit': 9,
        'account_number': 898787,
        'account_digit': 1,
        'company_name': 'VERT CAPITAL',
        'file_sequencial_number': 1,
        'company_reserved': '',
    }
    header = cnab_bradesco.header
    header.set_attributes(**data)

    cnab_bradesco._export_block(buffer, header)

    # TODO: assert export result
    assert True
