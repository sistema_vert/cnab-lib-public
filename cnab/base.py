from decimal import Decimal

from .exceptions import RequiredFieldError

NUMERIC = 'N'
ALPHANUMERIC = 'A'


class Field:
    _value = ''
    _formatter = None

    def __init__(self, name, code, size, description, position, **kwargs):
        self.name = name
        self.code = code
        self.size = size
        self.description = description
        self.position = position
        self.required = kwargs.get('required')
        self.formatter = kwargs.get('formatter')
        self.decimal_places = kwargs.get('decimal_places', 0)

    def __repr__(self):
        return self.value

    @property
    def formatter(self):
        return self._formatter

    @formatter.setter
    def formatter(self, value):
        if value.upper() not in (ALPHANUMERIC, NUMERIC):
            raise ValueError(f'{value} is not a valid choice use {ALPHANUMERIC} or {NUMERIC}')

        self._formatter = value

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        if self.decimal_places and not isinstance(value, (Decimal, float)):
            raise ValueError(f'field {self.name} - {self.code} - {value} is not a decimal instance')

        value = str(value)

        if len(value) > self.size:
            raise ValueError(f'Invalid size for field {self.name} - {self.code} - {self.description}')

        self._value = value

    def serialize(self):
        if self.required and not self.value:
            raise RequiredFieldError(f'field {self.name} - {self.code} - {self.description} is required')

        if self.value and self.formatter == NUMERIC:
            if self.decimal_places:
                amount, decimal_places = self.value.split('.')
                return amount.zfill(self.size) + decimal_places.zfill(self.decimal_places)
            return self.value.zfill(self.size)

        return self.value.ljust(self.size + self.decimal_places)


class BaseSpec:

    def __init__(self, fields) -> None:
        self.fields = fields
        

    def __new__(cls, fields):
        instance = super().__new__(cls)
        instance._configure_fields(fields_spec=fields)
        return instance

    def _configure_fields(self, fields_spec):
        for field_name, field_spec in fields_spec.items():
            default = field_spec.get('default', None)
            field_spec['name'] = field_name

            field = Field(**field_spec)
            if default:
                setattr(field, 'value', default)

            setattr(self, field_name, field)
            if field_name not in self.fields:
                self.fields.append(field_name)

    def set_attributes(self, **kwargs):
        for field_name, value in kwargs.items():
            if hasattr(self, field_name) and not isinstance(value, Field):
                field = getattr(self, field_name)
                field.value = value
                setattr(self, field_name, field)
                continue
            setattr(self, field_name, value)


class Batch:

    def __init__(self, header, trailer):
        self.header = header
        self.trailer = trailer
        self.payments = []

    def add_payment(self, payment):
        if not isinstance(payment, (Transfer, Bankslip)):
            raise ValueError('payment added is not a valid payment')

        self.payments.append(payment)


class FileTrailer(BaseSpec):
    fields = []


class FileHeader(BaseSpec):
    fields = []

class BatchHeader(BaseSpec):
    fields = []


class Transfer(BaseSpec):
    fields = []
    detail = None

    def add_detail(self, detail):
        if not isinstance(detail, SegmentB):
            raise ValueError('detail is not a valid segment b record')

        self.detail = detail


class Segment(BaseSpec):
    """
    Generic segment
    """
    fields = []


class SegmentB(BaseSpec):
    fields = []


class Bankslip(BaseSpec):
    fields = []
    detail = None

    def add_detail(self, detail):
        if not isinstance(detail, SegmentJ52):
            raise ValueError('detail is not a valid segment j52 record')

        self.detail = detail


class SegmentJ52(BaseSpec):
    fields = []


class BatchTrailer(BaseSpec):
    fields = []
