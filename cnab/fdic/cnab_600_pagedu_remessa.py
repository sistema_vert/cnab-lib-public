

SPEC = {
    "service": "",
    "version": "remessa",
    "layout": "cnab600",
    "header": {
        "fields": {
            "id_registro": {
                "description": "Identificação do Registro",
                "code": "1",
                "size": 1,
                "position": (1, 1),
                "formatter": "N",
                "required": True,
                "detail": '0'
            },
            "id_arquivo_remessa": {
                "description": "Identificação do Arquivo Remessa",
                "code": "2",
                "size": 1,
                "position": (2, 2),
                "formatter": "N",
                "required": True,
                "detail": "1"
            },
            "lit_remessa": {
                "description": "Literal Remessa",
                "code": "3",
                "size": 7,
                "position": (3, 9),
                "formatter": "A",
                "required": True,
                "detail": "REMESSA"
            },
            "codigo_servico": {
                "description": "Código de Serviço",
                "code": "4",
                "size": 2,
                "position": (10, 11),
                "formatter": "N",
                "required": True,
                "detail": "01"
            },
            "lit_servico": {
                "description": "Literal Serviço",
                "code": "5",
                "size": 15,
                "position": (12, 26),
                "formatter": "A",
                "required": True,
                "detail": "COBRANCA"
            },
            "codigo_cedente": {
                "description": "Código do Cedente",
                "code": "6",
                "size": 20,
                "position": (27, 46),
                "formatter": "N",
                "required": True,
                "detail": "Será fornecido pelo Banco, quando do Cadastramento"
            },
            "nome_cedente": {
                "description": "Nome do Cedente",
                "code": "7",
                "size": 30,
                "position": (47, 76),
                "formatter": "A",
                "required": True,
                "detail": "Razão Social"
            },
            "numero_banco_camara_compensacao": {
                "description": "Número do Banco na Câmara de Compensação",
                "code": "8",
                "size": 3,
                "position": (77, 79),
                "formatter": "N",
                "required": True,
                "detail": ""
            },
            "nome_banco": {
                "description": "Nome do Banco",
                "code": "9",
                "size": 15,
                "position": (80, 94),
                "formatter": "A",
                "required": True,
                "detail": ""
            },
            "data_gravacao_arquivo": {
                "description": "Data da Gravação do Arquivo",
                "code": "10",
                "size": 8,
                "position": (95, 102),
                "formatter": "N",
                "required": True,
                "detail": "DDMMAAAA"
            },
            "branco_1": {
                "description": "Branco",
                "code": "11",
                "size": 5,
                "position": (103, 107),
                "formatter": "A",
                "required": True,
                "detail": ""
            },
            "id_sistema": {
                "description": "Identificação do Sistema",
                "code": "12",
                "size": 3,
                "position": (108, 110),
                "formatter": "A",
                "required": True,
                "detail": "SFR"
            },
            "num_seq_arquivo": {
                "description": "Nº Seqüencial do Arquivo",
                "code": "13",
                "size": 7,
                "position": (111, 117),
                "formatter": "N",
                "required": True,
                "detail": "Seqüencial"
            },
            "data_cessao": {
                "description": "Data da Cessão",
                "code": "14",
                "size": 8,
                "position": (118, 125),
                "formatter": "N",
                "required": True,
                "detail": "DDMMAAAA"
            },
            "coobrigacao": {
                "description": "Coobrigação",
                "code": "15",
                "size": 1,
                "position": (126, 126),
                "formatter": "A",
                "required": False,
                "detail": "S / N"
            },
            "natureza_operacao": {
                "description": "Natureza da Operação",
                "code": "16",
                "size": 1,
                "position": (127, 127),
                "formatter": "N",
                "required": False,
                "detail": "Natureza da Operação"
            },
            "segmento": {
                "description": "Segmento",
                "code": "17",
                "size": 3,
                "position": (128, 130),
                "formatter": "A",
                "required": False,
                "detail": "Segmento"
            },
            "tipo_calculo": {
                "description": "Tipo de Cálculo",
                "code": "18",
                "size": 1,
                "position": (131, 131),
                "formatter": "N",
                "required": False,
                "detail": "Segmento"
            },
            "branco_2": {
                "description": "Branco",
                "code": "19",
                "size": 463,
                "position": (132, 594),
                "formatter": "A",
                "required": True,
                "detail": "Branco"
            },
            "num_seq_registro": {
                "description": "Nº Seqüencial do Registro (de 1 em 1)",
                "code": "20",
                "size": 6,
                "position": (565, 600),
                "formatter": "N",
                "required": True,
                "detail": "000001"
            }
        }
    }, 
    "trailer":{
        "fields":{
            "id_registro":{
                "description": "Identificação Registro",
                "code": "1",
                "size": 1,
                "position": (1, 1),
                "formatter": "N",
                "required": True,
                "detail": "9"
            },
            "branco": {
                "description": "Branco",
                "code": "2",
                "size": 593,
                "position": (2, 594),
                "formatter": "A",
                "required": True,
                "detail": "Branco"
            },
            "num_seq_registro":{
                "description": "Número sequencial de Registro",
                "code": "3",
                "size": 6,
                "position": (595, 600),
                "formatter": "N",
                "required": True,
                "detail": "Nº Seqüencial do Último Registro"
            }
        }
    },
    "batch": None,
    "statements": {
        "segment":{
            "id_registro":{
                "description": "Identificação do Registro",
                "code": "1",
                "size": 1,
                "position": (1, 1),
                "formatter": "N",
                "required": True,
                "detail": "1"
            },
            "tipo_inscricao_cedente":{
                "description": "Tipo de Inscrição do Cedente",
                "code": "2",
                "size": 2,
                "position": (2, 3),
                "formatter": "N",
                "required": True,
                "detail": "Tipo de Inscrição do Cedente"
            },
            "num_inscricao_cedente":{
                "description": "Nº de Inscrição do Cedente",
                "code": "3",
                "size": 14,
                "position": (4, 17),
                "formatter": "N",
                "required": True,
                "detail": "CNPJ / CPF"
            },
            "num_contrato":{
                "description": "Nº do Contrato",
                "code": "4",
                "size": 12,
                "position": (18, 29),
                "formatter": "N",
                "required": True,
                "detail": "Nº do Contrato"
            },
            "num_parcela":{
                "description": "Número da Parcela",
                "code": "5",
                "size": 3,
                "position": (30, 32),
                "formatter": "N",
                "required": True,
                "detail": "Nº da parcela no contrato"
            },
            "total_parcelas":{
                "description": "Total de Parcelas",
                "code": "6",
                "size": 3,
                "position": (33, 35),
                "formatter": "N",
                "required": True,
                "detail": "Total de parcelas do contrato"
            },
            "brancos_1":{
                "description": "Brancos",
                "code": "7",
                "size": 12,
                "position": (36, 47),
                "formatter": "A",
                "required": False,
                "detail": "Brancos"
            },
            "seu_numero":{
                "description": "Seu número",
                "code": "8",
                "size": 25,
                "position": (48, 72),
                "formatter": "A",
                "required": False,
                "detail": "Número de identificação do título"
            },
            "zeros_1":{
                "description": "Zeros",
                "code": "9",
                "size": 11,
                "position": (73, 83),
                "formatter": "N",
                "required": False,
                "detail": "Zeros"
            },
            "id_titulo_banco":{
                "description": "Identificação do Título no Banco",
                "code": "10",
                "size": 11,
                "position": (84, 94),
                "formatter": "A",
                "required": False,
                "detail": "Nosso Número no Banco Cobrador"
            },
            "digito_nosso_numero":{
                "description": "Dígito do Nosso Número",
                "code": "11",
                "size": 1,
                "position": (95, 95),
                "formatter": "A",
                "required": False,
                "detail": "Dígito de Auto Conferência do Nosso Número"
            },
            "valor_pago":{
                "description": "Valor pago",
                "code": "12",
                "size": 13,
                "position": (96, 108),
                "formatter": "N",
                "decimal_places": 2,
                "required": False,
                "detail": "Valor pago na liquidação/baixa do título."
            },
            "data_liquidacao":{
                "description": "Data da Liquidação",
                "code": "13",
                "size": 8,
                "position": (109, 116),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "data_obito":{
                "description": "Data de Óbito",
                "code": "14",
                "size": 8,
                "position": (117, 124),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "brancos_2":{
                "description": "Brancos",
                "code": "15",
                "size": 2,
                "position": (125, 126),
                "formatter": "A",
                "required": False,
                "detail": "Brancos"
            },
            "codigo_loja":{
                "description": "Código de Loja",
                "code": "16",
                "size": 3,
                "position": (127, 129),
                "formatter": "N",
                "required": False,
                "detail": "Código da Loja"
            },
            "codigo_produto":{
                "description": "Código de Produto",
                "code": "17",
                "size": 3,
                "position": (130, 132),
                "formatter": "N",
                "required": False,
                "detail": "Código do Produto"
            },
            "codigo_tipo_financiamento":{
                "description": "Código de Tipo de Financiamento",
                "code": "18",
                "size": 3,
                "position": (133, 135),
                "formatter": "N",
                "required": False,
                "detail": "Código do Tipo de Financiamento"
            },
            "codigo_setor_sacado":{
                "description": "Código de Setor de Sacado",
                "code": "19",
                "size": 3,
                "position": (136, 138),
                "formatter": "N",
                "required": False,
                "detail": "Código de Setor de Sacado"
            },
            "id_ocorrencia":{
                "description": "Identificação Ocorrência",
                "code": "20",
                "size": 2,
                "position": (139, 140),
                "formatter": "N",
                "required": False,
                "detail": "Códigos de Ocorrência"
            },
            "data_vencimento_titulo":{
                "description": "Data do Vencimento do Título",
                "code": "21",
                "size": 8,
                "position": (141, 148),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "valor_titulo":{
                "description": "Valor do Título",
                "code": "22",
                "size": 13,
                "position": (149, 161),
                "formatter": "N",
                "decimal_places": 2,
                "required": False,
                "detail": "Valor do Título (preencher sem ponto e sem vírgula)"
            },
            "banco_encarregado_cobranca":{
                "description": "Banco Encarregado da Cobrança",
                "code": "23",
                "size": 3,
                "position": (162, 164),
                "formatter": "N",
                "required": False,
                "detail": "Nº do Banco na Câmara de Compensação"
            },
            "agencia_depositaria":{
                "description": "Agência Depositária",
                "code": "24",
                "size": 5,
                "position": (165, 169),
                "formatter": "N",
                "required": False,
                "detail": "Código da Agência Depositária"
            },
            "seq_renegociacao":{
                "description": "Sequencial de renegociação",
                "code": "25",
                "size": 8,
                "position": (170, 177),
                "formatter": "A",
                "required": False,
                "detail": "Número indicador da renegociação"
            },
            "brancos_3":{
                "description": "Brancos",
                "code": "26",
                "size": 2,
                "position": (178, 179),
                "formatter": "A",
                "required": False,
                "detail": "Brancos"
            },
            "especie_titulo":{
                "description": "Espécie de Título",
                "code": "27",
                "size": 2,
                "position": (180, 181),
                "formatter": "N",
                "required": False,
                "detail": "00-Duplicata Mercantil / 01-Duplicata / 02-Nota Promissória / 03-Recibo / 04-Nota de Seguro / 05-Cheque / 06-Duplicata de Serviço / 07-Letras de Câmbio / 08-CPR Rural / 10-CCB / 11-Parcela de Contrato / 99-Outros"
            },
            "identificacao":{
                "description": "Identificação",
                "code": "28",
                "size": 1,
                "position": (182, 182),
                "formatter": "A",
                "required": False,
                "detail": "A - aceito / N - não aceito"
            },
            "data_emissao_titulo":{
                "description": "Data da emissão do Título",
                "code": "29",
                "size": 8,
                "position": (183, 190),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "primeira_instrucao":{
                "description": "1ª instrução",
                "code": "30",
                "size": 2,
                "position": (191, 192),
                "formatter": "N",
                "required": False,
                "detail": "1ª instrução"
            },
            "segunda_instrucao":{
                "description": "2ª instrução",
                "code": "31",
                "size": 2,
                "position": (193, 194),
                "formatter": "N",
                "required": False,
                "detail": "2ª instrução"
            },
            "valor_dia_atraso":{
                "description": "Valor a ser cobrado por dia de Atraso",
                "code": "32",
                "size": 13,
                "position": (195, 207),
                "formatter": "N",
                "required": False,
                "detail": "Valor a ser cobrado por Dia Atraso"
            },
            "data_limite_desconto":{
                "description": "Data Limite de Desconto",
                "code": "33",
                "size": 8,
                "position": (208, 215),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "valor_desconto":{
                "description": "Valor do Desconto",
                "code": "34",
                "size": 13,
                "position": (216, 228),
                "formatter": "N",
                "required": False,
                "detail": "Valor Desconto"
            },
            "valor_presente_parcela":{
                "description": "Valor Presente da Parcela",
                "code": "35",
                "size": 13,
                "position": (229, 241),
                "formatter": "N",
                "decimal_places": 2,
                "required": False,
                "detail": "Valor da parcela na data que foi cedida"
            },
            "valor_abatimento":{
                "description": "Valor do Abatimento",
                "code": "36",
                "size": 13,
                "position": (242, 254),
                "formatter": "N",
                "decimal_places": 2,
                "required": False,
                "detail": "Valor do Abatimento a ser concedido ou cancelado"
            },
            "indice":{
                "description": "Índice",
                "code": "37",
                "size": 3,
                "position": (255, 257),
                "formatter": "N",
                "required": False,
                "detail": "Índice"
            },
            "correcao_indice":{
                "description": "Correção do Índice",
                "code": "38",
                "size": 7,
                "position": (258, 264),
                "formatter": "N",
                "required": False,
                "detail": "Percentual de Correção do Índice"
            },
            "id_tipo_inscricao_sacado": {
                "description": "Identificação do tipo de Inscrição do Sacado",
                "code": "39",
                "size": 2,
                "position": (265, 266),
                "formatter": "N",
                "required": False,
                "detail": "01-CPF / 02-CNPJ"
            },
            "num_inscricao_sacado": {
                "description": "Nº Inscrição do Sacado",
                "code": "40",
                "size": 14,
                "position": (267, 280),
                "formatter": "N",
                "required": False,
                "detail": "CNPJ/CPF"
            },
            "nome_sacado": {
                "description": "Nome do Sacado",
                "code": "41",
                "size": 40,
                "position": (281, 320),
                "formatter": "A",
                "required": False,
                "detail": "Nome do Sacado"
            },
            "endereco_completo": {
                "description": "Endereço Completo",
                "code": "42",
                "size": 40,
                "position": (321, 360),
                "formatter": "A",
                "required": False,
                "detail": "Endereço do Sacado"
            },
            "primeira_mensagem": {
                "description": "1ª Mensagem",
                "code": "43",
                "size": 12,
                "position": (361, 372),
                "formatter": "A",
                "required": False,
                "detail": "1ª Mensagem"
            },
            "cep": {
                "description": "CEP",
                "code": "44",
                "size": 8,
                "position": (373, 380),
                "formatter": "N",
                "required": False,
                "detail": "CEP do Sacado"
            },
            "data_nascimento_sacado": {
                "description": "Data Nascimento do Sacado",
                "code": "45",
                "size": 8,
                "position": (381, 388),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "idade_sacado": {
                "description": "Idade do Sacado",
                "code": "46",
                "size": 3,
                "position": (389, 391),
                "formatter": "N",
                "required": False,
                "detail": "Idade do Sacado"
            },
            "tipo_inscricao_sacador_avalista": {
                "description": "Tipo de Inscrição do Sacador Avalista",
                "code": "47",
                "size": 2,
                "position": (392, 393),
                "formatter": "N",
                "required": False,
                "detail": "01-CPF / 02-CNPJ"
            },
            "num_inscricao_sacador_avalista": {
                "description": "Nº de Inscrição do Sacador Avalista",
                "code": "48",
                "size": 14,
                "position": (394, 407),
                "formatter": "N",
                "required": False,
                "detail": "CNPJ / CPF"
            },
            "nome_sacador_avalista": {
                "description": "Nome do Sacador Avalista",
                "code": "49",
                "size": 40,
                "position": (408, 447),
                "formatter": "A",
                "required": False,
                "detail": "Nome do Sacador Avalista"
            },
            "valor_financiado": {
                "description": "Valor Financiado",
                "code": "50",
                "size": 13,
                "position": (448, 460),
                "formatter": "N",
                "required": False,
                "detail": "Valor Financiado"
            },
            "matricula_sacado": {
                "description": "Matrícula do sacado",
                "code": "51",
                "size": 12,
                "position": (461, 472),
                "formatter": "N",
                "required": False,
                "detail": "Matrícula do sacado no ente consignante"
            },
            "cnpj_ente_consignante": {
                "description": "CNPJ do Ente Consignante",
                "code": "52",
                "size": 14,
                "position": (473, 486),
                "formatter": "N",
                "required": False,
                "detail": "CNPJ do ente consignante"
            },
            "tipo_contrato": {
                "description": "Tipo de Contrato",
                "code": "53",
                "size": 3,
                "position": (487, 489),
                "formatter": "N",
                "required": False,
                "detail": "Tipo de Contrato (C3)"
            },
            "tipo_ativo": {
                "description": "Tipo de Ativo",
                "code": "54",
                "size": 1,
                "position": (490, 490),
                "formatter": "A",
                "required": False,
                "detail": "P-Performado / N-Não Performado"
            },
            "nfe": {
                "description": "NF-e",
                "code": "55",
                "size": 44,
                "position": (491, 534),
                "formatter": "A",
                "required": False,
                "detail": "Nota Fiscal Eletrônica"
            },
            "sexo_sacado": {
                "description": "Sexo do Sacado",
                "code": "56",
                "size": 1,
                "position": (535, 535),
                "formatter": "A",
                "required": False,
                "detail": "M-Masculino / F-Feminino"
            },
            "nacionalidade_sacado": {
                "description": "Nacionalidade do Sacado",
                "code": "57",
                "size": 1,
                "position": (536, 536),
                "formatter": "A",
                "required": False,
                "detail": "1 – Brasileira / 2 - Outras"
            },
            "estado_civil_sacado": {
                "description": "Estado Civil do Sacado",
                "code": "58",
                "size": 1,
                "position": (537, 537),
                "formatter": "N",
                "required": False,
                "detail": "1 - Solteiro / 2 - Casado / 3 - Divorciado / 4 - Separado / 5 - Viúvo / 6 - União estável"
            },
            "rg_sacado": {
                "description": "RG do sacado",
                "code": "59",
                "size": 11,
                "position": (538, 548),
                "formatter": "N",
                "required": False,
                "detail": ""
            },
            "data_emissao_rg": {
                "description": "Data de emissão RG",
                "code": "60",
                "size": 8,
                "position": (549, 556),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "valor_iof_entrada": {
                "description": "Valor IOF Entrada",
                "code": "61",
                "size": 11,
                "position": (557, 567),
                "formatter": "N",
                "required": False,
                "detail": ""
            },
            "taxa_contrato": {
                "description": "Taxa do Contrato",
                "code": "62",
                "size": 15,
                "position": (568, 582),
                "formatter": "N",
                "required": False,
                "detail": ""
            },
            "brancos_4": {
                "description": "Brancos",
                "code": "63",
                "size": 12,
                "position": (583, 594),
                "formatter": "A",
                "required": False,
                "detail": "Brancos"
            },
            "num_seq_registro": {
                "description": "Nº Seqüencial do Registro",
                "code": "64",
                "size": 12,
                "position": (595, 600),
                "formatter": "N",
                "required": False,
                "detail": "Nº Seqüencial do Registro"
            },
        }
    }

}

from ..helpers import get_value


def get_segments(record_lines):
    layout = SPEC["statements"]["segment"]
    return [
        {k: get_value(line, spec) for k, spec in layout.items()}
        for line in record_lines
    ]