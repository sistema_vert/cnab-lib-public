
SPEC = {
    "service": "",
    "version": "retorno",
    "layout": "cnab600",
    "header": {
        "fields":{
            "id_registro":{
                "description": "Identificação do Registro",
                "code": "1",
                "size": 1,
                "position": (1, 1),
                "formatter": "N",
                "required": True,
                "detail": "0"
            },
            "id_arquivo_retorno":{
                "description": "Identificação do Arquivo Retorno",
                "code": "2",
                "size": 1,
                "position": (2, 2),
                "formatter": "N",
                "required": True,
                "detail": "2"
            },
            "lit_retorno":{
                "description": "Literal Retorno",
                "code": "3",
                "size": 7,
                "position": (3, 9),
                "formatter": "A",
                "required": True,
                "detail": "RETORNO"
            },
            "codigo_servico":{
                "description": "Código do Serviço",
                "code": "4",
                "size": 2,
                "position": (10, 11),
                "formatter": "N",
                "required": True,
                "detail": "01"
            },
            "lit_servico":{
                "description": "Literal Serviço",
                "code": "5",
                "size": 15,
                "position": (12, 26),
                "formatter": "A",
                "required": True,
                "detail": "COBRANÇA"
            },
            "codigo_carteira_cliente":{
                "description": "Código da Carteira / Cliente",
                "code": "6",
                "size": 20,
                "position": (27, 46),
                "formatter": "N",
                "required": True,
                "detail": "Nº Empresa"
            },
            "nome_cedente_extenso":{
                "description": "Nome do Cedente por Extenso",
                "code": "7",
                "size": 30,
                "position": (47, 76),
                "formatter": "A",
                "required": True,
                "detail": "Razão Social"
            },
            "num_banco_camara_compensacao":{
                "description": "Nº do Banco na Câmara de Compensação",
                "code": "8",
                "size": 3,
                "position": (77, 79),
                "formatter": "N",
                "required": True,
                "detail": ""
            },
            "nome_banco_extenso":{
                "description": "Nome do Banco por Extenso",
                "code": "9",
                "size": 15,
                "position": (80, 94),
                "formatter": "A",
                "required": True,
                "detail": "Nome do banco"
            },
            "data_gravacao_arquivo":{
                "description": "Data da Gravação do Arquivo",
                "code": "10",
                "size": 8,
                "position": (95, 102),
                "formatter": "N",
                "required": True,
                "detail": "DDMMAAAA"
            },
            "zeros_1":{
                "description": "Zeros",
                "code": "11",
                "size": 5,
                "position": (103, 107),
                "formatter": "N",
                "required": True,
                "detail": "Zeros"
            },
            "zeros_2":{
                "description": "Zeros",
                "code": "12",
                "size": 3,
                "position": (108, 110),
                "formatter": "N",
                "required": True,
                "detail": "Zeros"
            },
            "num_seq_arquivo":{
                "description": "Nº Seqüencial do Arquivo",
                "code": "13",
                "size": 7,
                "position": (111, 117),
                "formatter": "N",
                "required": True,
                "detail": "Seqüencial do Arquivo"
            },
            "data_cessao":{
                "description": "Data da Cessão",
                "code": "14",
                "size": 6,
                "position": (118, 125),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "data_liquidacao_cessao":{
                "description": "Data Liquidação Cessão",
                "code": "15",
                "size": 6,
                "position": (126, 133),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "branco":{
                "description": "Branco",
                "code": "16",
                "size": 461,
                "position": (134, 594),
                "formatter": "A",
                "required": False,
                "detail": "Branco"
            },
            "num_seq_registro":{
                "description": "Nº Seqüencial de registro",
                "code": "17",
                "size": 6,
                "position": (595, 600),
                "formatter": "N",
                "required": True,
                "detail": "000001"
            }
        }
    },
    "trailer": {
        "fields": {
            "id_registro":{
                "description": "Identificação do Registro",
                "code": "1",
                "size": 1,
                "position": (1, 1),
                "formatter": "N",
                "required": True,
                "detail": "9"
            },
            "brancos":{
                "description": "Brancos",
                "code": "8",
                "size": 593,
                "position": (2, 594),
                "formatter": "A",
                "required": False,
                "detail": "Brancos"
            },
            "num_seq_registro":{
                "description": "Número Seqüencial do Registro",
                "code": "9",
                "size": 6,
                "position": (595, 600),
                "formatter": "N",
                "required": True,
                "detail": "Nº Seqüencial do Registro"
            },
        }
    },
    "batch": None,
    "statements": {
        "segments": {
            "id_registro":{
                "description": "Identificação do Registro",
                "code": "1",
                "size": 1,
                "position": (1, 1),
                "formatter": "N",
                "required": True,
                "detail": "1"
            },
            "tipo_inscricao_cedente":{
                "description": "Tipo de Inscrição do Cedente",
                "code": "2",
                "size": 2,
                "position": (2, 3),
                "formatter": "N",
                "required": True,
                "detail": "Tipo de Inscrição do Cedente"
            },
            "num_inscricao_cedente":{
                "description": "Nº de Inscrição do Cedente",
                "code": "3",
                "size": 14,
                "position": (4, 17),
                "formatter": "N",
                "required": True,
                "detail": "CNPJ / CPF"
            },
            "num_contrato":{
                "description": "Nº do Contrato",
                "code": "4",
                "size": 12,
                "position": (18, 29),
                "formatter": "A",
                "required": True,
                "detail": "Nº do Contrato"
            },
            "num_parcela":{
                "description": "Número da Parcela",
                "code": "5",
                "size": 3,
                "position": (30, 32),
                "formatter": "N",
                "required": True,
                "detail": "Nº da parcela no contrato"
            },
            "total_parcelas":{
                "description": "Total de Parcelas",
                "code": "6",
                "size": 3,
                "position": (33, 35),
                "formatter": "N",
                "required": True,
                "detail": "Total de parcelas do contrato"
            },
            "brancos_1":{
                "description": "Brancos",
                "code": "7",
                "size": 12,
                "position": (36, 47),
                "formatter": "A",
                "required": True,
                "detail": "Brancos"
            },
            "seu_numero":{
                "description": "Seu número",
                "code": "8",
                "size": 25,
                "position": (48, 72),
                "formatter": "A",
                "required": True,
                "detail": "Número de identificação do título no Banco"
            },
            "zeros_1":{
                "description": "Zeros",
                "code": "9",
                "size": 11,
                "position": (73, 83),
                "formatter": "N",
                "required": True,
                "detail": "Zeros"
            },
            "id_titulo_banco":{
                "description": "Identificação do Título no Banco",
                "code": "10",
                "size": 11,
                "position": (84, 94),
                "formatter": "N",
                "required": False,
                "detail": "Nosso Número no Banco Cobrador"
            },
            "digito_nosso_numero":{
                "description": "Dígito do Nosso Número",
                "code": "11",
                "size": 1,
                "position": (95, 95),
                "formatter": "A",
                "required": False,
                "detail": "Dígito de Auto Conferência do Nosso Número"
            },
            "valor_pago":{
                "description": "Valor pago",
                "code": "12",
                "size": 13,
                "position": (96, 108),
                "formatter": "N",
                "decimal_places": 2,
                "required": False,
                "detail": "Valor pago na liquidação/baixa do título."
            },
            "data_liquidacao":{
                "description": "Data da Liquidação",
                "code": "13",
                "size": 8,
                "position": (109, 116),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "data_obito":{
                "description": "Data de Óbito",
                "code": "14",
                "size": 8,
                "position": (117, 124),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "brancos_2":{
                "description": "Brancos",
                "code": "15",
                "size": 2,
                "position": (125, 126),
                "formatter": "A",
                "required": True,
                "detail": "Brancos"
            },
            "codigo_loja":{ # este tem o mesmo code que o brancos2 na documentação
                "description": "Código de Loja",
                "code": "16",
                "size": 3,
                "position": (127, 129),
                "formatter": "N",
                "required": True,
                "detail": "Código da Loja"
            },
            "codigo_produto":{
                "description": "Código de Produto",
                "code": "17",
                "size": 3,
                "position": (130, 132),
                "formatter": "N",
                "required": True,
                "detail": "Código de Produto"
            },
            "codigo_tipo_financiamento":{
                "description": "Código de Tipo de Financiamento",
                "code": "18",
                "size": 3,
                "position": (133, 135),
                "formatter": "N",
                "required": False,
                "detail": "Código do Tipo de Financiamento"
            },
            "codigo_setor_sacado":{
                "description": "Código de Setor de Sacado",
                "code": "19",
                "size": 3,
                "position": (136, 138),
                "formatter": "N",
                "required": False,
                "detail": "Código de Setor de Sacado"
            },
            "id_ocorrencia":{
                "description": "Identificação Ocorrência",
                "code": "20",
                "size": 2,
                "position": (139, 140),
                "formatter": "N",
                "required": True,
                "detail": "Códigos de Ocorrência"
            },
            "data_vencimento_titulo":{
                "description": "Data do Vencimento do Título",
                "code": "21",
                "size": 8,
                "position": (141, 148),
                "formatter": "N",
                "required": True,
                "detail": "DDMMAAAA"
            },
            "valor_titulo":{
                "description": "Valor do Título",
                "code": "22",
                "size": 13,
                "position": (149, 161),
                "formatter": "N",
                "decimal_places": 2,
                "required": True,
                "detail": "Valor do Título (Sem ponto e sem vírgula)"
            },
            "banco_encarregado_cobranca":{
                "description": "Banco Encarregado da Cobrança",
                "code": "23",
                "size": 3,
                "position": (162, 164),
                "formatter": "N",
                "required": False,
                "detail": "Nº do Banco na Câmara de Compensação"
            },
            "agencia_depositaria":{
                "description": "Agência Depositária",
                "code": "24",
                "size": 5,
                "position": (165, 169),
                "formatter": "N",
                "required": False,
                "detail": "Código da Agência Depositária"
            },
            "brancos_3":{
                "description": "Brancos",
                "code": "25",
                "size": 10,
                "position": (170, 179),
                "formatter": "A",
                "required": True,
                "detail": "Brancos"
            },
            "especie_titulo":{
                "description": "Espécie de Título",
                "code": "26",
                "size": 2,
                "position": (180, 181),
                "formatter": "N",
                "required": True,
                "detail": "00-Duplicata Mercantil / 01-Duplicata / 02-Nota Promissória / 03-Recibo / 04-Nota de Seguro / 05-Cheque / 06-Duplicata de Serviço / 07-Letras de Câmbio / 08-CPR Rural / 10-CCB / 11-Parcela de Contrato / 99-Outros"
            },
            "codigo_aceite":{
                "description": "Código de Aceite",
                "code": "27",
                "size": 1,
                "position": (182, 182),
                "formatter": "A",
                "required": True,
                "detail": "A - aceito / N - não aceito"
            },
            "data_emissao_titulo":{
                "description": "Data da emissão do Título",
                "code": "28",
                "size": 8,
                "position": (183, 190),
                "formatter": "N",
                "required": True,
                "detail": "DDMMAAAA"
            },
            "primeira_instrucao":{
                "description": "1ª instrução",
                "code": "29",
                "size": 2,
                "position": (191, 192),
                "formatter": "N",
                "required": False,
                "detail": "1ª instrução"
            },
            "segunda_instrucao":{
                "description": "2ª instrução",
                "code": "30",
                "size": 2,
                "position": (193, 194),
                "formatter": "N",
                "required": False,
                "detail": "2ª instrução"
            },
            "valor_dia_atraso":{
                "description": "Valor a ser cobrado por dia de Atraso",
                "code": "31",
                "size": 13,
                "position": (195, 207),
                "formatter": "N",
                "required": False,
                "detail": "Valor a ser cobrado por Dia Atraso"
            },
            "data_limite_desconto":{
                "description": "Data Limite de Desconto",
                "code": "32",
                "size": 8,
                "position": (208, 215),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "valor_desconto":{
                "description": "Valor do Desconto",
                "code": "33",
                "size": 13,
                "position": (216, 228),
                "formatter": "N",
                "required": False,
                "detail": "Valor Desconto"
            },
            "valor_presente_parcela":{
                "description": "Valor Presente da Parcela",
                "code": "34",
                "size": 13,
                "position": (229, 241),
                "formatter": "N",
                "decimal_places": 2,
                "required": True,
                "detail": "Valor da parcela na data que foi cedida"
            },
            "valor_abatimento":{
                "description": "Valor do Abatimento",
                "code": "35",
                "size": 13,
                "position": (242, 254),
                "formatter": "N",
                "decimal_places": 2,
                "required": False,
                "detail": "Valor do Abatimento a ser concedido ou cancelado"
            },
            "indice":{
                "description": "Índice",
                "code": "36",
                "size": 3,
                "position": (255, 257),
                "formatter": "N",
                "required": True,
                "detail": "Índice"
            },
            "correcao_indice":{
                "description": "Correção do Índice",
                "code": "37",
                "size": 7,
                "position": (258, 264),
                "formatter": "N",
                "decimal_places": 4,
                "required": False,
                "detail": "Percentual de Correção do Índice"
            },
            "id_tipo_inscricao_sacado":{
                "description": "Identificação do tipo de Inscrição do Sacado",
                "code": "38",
                "size": 2,
                "position": (265, 266),
                "formatter": "N",
                "required": True,
                "detail": "01-CPF / 02-CNPJ"
            },
            "num_inscricao_sacado":{
                "description": "Nº Inscrição do Sacado",
                "code": "39",
                "size": 14,
                "position": (267, 280),
                "formatter": "N",
                "required": True,
                "detail": "CNPJ/CPF"
            },
            "nome_sacado":{
                "description": "Nome do Sacado",
                "code": "40",
                "size": 40,
                "position": (281, 320),
                "formatter": "A",
                "required": True,
                "detail": "Nome do Sacado"
            },
            "endereco_completo":{
                "description": "Endereço Completo",
                "code": "41",
                "size": 40,
                "position": (321, 360),
                "formatter": "A",
                "required": True,
                "detail": "Endereço do Sacado"
            },
            "primeira_mensagem":{
                "description": "1ª Mensagem",
                "code": "42",
                "size": 12,
                "position": (361, 372),
                "formatter": "A",
                "required": False,
                "detail": "1ª Mensagem"
            },
            "cep":{
                "description": "CEP",
                "code": "43",
                "size": 8,
                "position": (373, 380),
                "formatter": "A",
                "required": True,
                "detail": "CEP do Sacado"
            },
            "data_nascimento_sacado":{
                "description": "Data Nascimento do Sacado",
                "code": "44",
                "size": 8,
                "position": (381, 388),
                "formatter": "N",
                "required": False,
                "detail": "DDMMAAAA"
            },
            "idade_sacado":{
                "description": "Idade do Sacado",
                "code": "45",
                "size": 3,
                "position": (389, 391),
                "formatter": "N",
                "required": False,
                "detail": "Idade do Sacado"
            },
            "valor_financiado":{
                "description": "Valor Financiado",
                "code": "46",
                "size": 13,
                "position": (392, 404),
                "formatter": "N",
                "required": False,
                "detail": "Valor Financiado"
            },
            "matricula_sacado":{
                "description": "Matrícula do sacado",
                "code": "47",
                "size": 12,
                "position": (405, 416),
                "formatter": "N",
                "required": False,
                "detail": "Matrícula do sacado no ente consignante"
            },
            "cnpj_ente_consignante":{
                "description": "CNPJ do Ente Consignante",
                "code": "48",
                "size": 14,
                "position": (417, 430),
                "formatter": "N",
                "required": False,
                "detail": "CNPJ do ente consignante"
            },
            "tipo_contrato":{
                "description": "Tipo de Contrato",
                "code": "49",
                "size": 3,
                "position": (431, 433),
                "formatter": "N",
                "required": False,
                "detail": "Tipo de Contrato"
            },
            "tipo_ativo":{
                "description": "Tipo de Ativo",
                "code": "50",
                "size": 1,
                "position": (434, 434),
                "formatter": "N",
                "required": False,
                "detail": "P-Performado / N-Não Performado"
            },
            "descricao_ocorrencia":{
                "description": "Descrição da Ocorrência",
                "code": "51",
                "size": 56,
                "position": (435, 490),
                "formatter": "A",
                "required": False,
                "detail": "Descrição da Ocorrência"
            },
            "nfe":{
                "description": "NF-e",
                "code": "52",
                "size": 44,
                "position": (491, 534),
                "formatter": "A",
                "required": False,
                "detail": "Nota Fiscal Eletrônica"
            },
            "brancos_4":{
                "description": "Brancos",
                "code": "53",
                "size": 60,
                "position": (535, 594),
                "formatter": "A",
                "required": False,
                "detail": "Brancos"
            },
            "num_seq_registro":{
                "description": "Nº Seqüencial do Registro",
                "code": "54",
                "size": 6,
                "position": (595, 600),
                "formatter": "N",
                "required": True,
                "detail": "Nº Seqüencial do Registro"
            }
        }
    }
}

from ..helpers import get_value

def get_segments(record_lines):
    layout = SPEC['statements']['segments']
    return [
        {k: get_value(line, spec) for k, spec in layout.items()} 
        for line in record_lines
    ]
