LAYOUTS = {
    "bradesco": {
        "layout": {
            "240": {
                "version": {
                    "04": {
                        "spec": "cnab.bradesco.cnab_240_04",
                    },
                    "05": {
                        "spec": "cnab.bradesco.cnab_240_05",
                    },
                }
            }
        }
    },
    "santander": {
        "layout": {
            "240": {
                "version": {
                    "01": {
                        "spec": "cnab.santander.cnab_240_01",
                    }
                }
            }
        }
    },
    "fdic": {
        "layout": {
            "500": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_500_remessa",
                    },
                    "retorno": {
                        "spec": "cnab.fdic.cnab_500_retorno",
                    },
                }
            },
            "500_koin": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_500_koin_remessa",
                    },
                    "retorno": {
                        "spec": "cnab.fdic.cnab_500_koin_retorno",
                    },
                }
            },
            "500_fusve": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_500_fusve_remessa",
                    },
                    "retorno": {
                        "spec": "cnab.fdic.cnab_500_fusve_retorno",
                    },
                }
            },
            "500_isaac": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_500_isaac_remessa",
                    },
                    "retorno": {
                        "spec": "cnab.fdic.cnab_500_isaac_retorno",
                    },
                }
            },
            "500_novo_mundo": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_500_novo_mundo_remessa",
                    },
                    "retorno": {
                        "spec": "cnab.fdic.cnab_500_novo_mundo_retorno",
                    },
                }
            },
            "500_ciasprev": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_500_ciasprev_remessa",
                    },
                    "retorno": {
                        "spec": "cnab.fdic.cnab_500_ciasprev_retorno",
                    },
                }
            },
            "600": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_600_dock99_remessa",
                    },
                    "retorno": {
                        "spec": "cnab.fdic.cnab_600_dock99_retorno",
                    },
                }
            },
            "600_capim": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_600_capim_remessa",
                    },
                    "retorno": {
                        "spec": "cnab.fdic.cnab_600_capim_retorno",
                    },
                }
            },
            "444_nau": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_444_nau_remessa",
                    },
                    "retorno": {
                        "spec": "cnab.fdic.cnab_444_nau_retorno",
                    },
                }
            },
            "444_money": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_444_money_remessa",
                    },
                    "retorno": {
                        "spec": "cnab.fdic.cnab_444_money_retorno",
                    },
                }
            },
            "600_pagedu": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_600_pagedu_remessa",
                    },
                    "retorno": {
                        "spec": "cnab.fdic.cnab_600_pagedu_retorno",
                    },
                }
            },
            "444_corres_banqueiro": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_444_correspondente_banqueiro_remessa",
                    },
                    "retorno": {
                        "spec": "",
                    },
                }
            },
            "444_cashu": {
                "version": {
                    "remessa": {
                        "spec": "cnab.fdic.cnab_444_cashu_remessa",
                    },
                    "retorno": {
                        "spec": "",
                    },
                }
            },
        }
    },
    "finaxis": {
        "layout": {
            "500": {
                "version": {
                    "5.06": {
                        "spec": "cnab.finaxis.cnab_500_remessa_5_06",
                    },
                }
            }
        }
    },
}
