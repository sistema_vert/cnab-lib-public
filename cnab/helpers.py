from datetime import datetime

import pytz

TIMEZONE = 'America/Sao_Paulo'


def now():
    """
    Get current date in timezone SAO PAULO.
    """
    now = datetime.now()
    return now.astimezone(pytz.timezone(TIMEZONE))


def get_value(line, field_spec):
    return line[field_spec["position"][0] - 1:field_spec["position"][1]].strip()
