# field type N(numeric) apayment_idlignment RIGHT and zeros to left
# field type ALFA(alfa numeric) alignment LEFT and empty to rigth
from cnab.helpers import now

from . import common

SPEC = {
    'service': 'cobranca',
    'version': '04',
    'layout': 'cnab240',
    'header': {
        'fields': {
            'bank_code': {
                'code': '01.0',
                'size': 3,
                'description': 'código do banco na compensação',
                'position': (1, 3),
                'formatter': 'N',
                'required': False,
                'default': 237
            },
            'batch_number': {
                'code': '02.0',
                'size': 4,
                'description': 'Lote de serviço',
                'position': (4, 7),
                'formatter': 'N',
                'required': True,
                'default': '0'
            },
            'registry_type': {
                'code': '03.0',
                'size': 1,
                'description': 'tipo de registro',
                'position': (8, 8),
                'formatter': 'N',
                'required': True,
                'default': '0'
            },
            'febraban_usage': {
                'code': '04.0',
                'size': 9,
                'description': 'Uso FEBRABAN',
                'position': (9, 17),
                'formatter': 'A',
                'required': False,
                'default': ''
            },
            'company_document_type': {
                'code': '05.0',
                'size': 1,
                'description': 'Tipo de inscrição da empresa',
                'position': (18, 18),
                'required': True,
                'formatter': 'N',
                'default': 2
            },
            'company_document_number': {
                'code': '06.0',
                'size': 14,
                'description': 'Número de inscrição da empresa',
                'position': (19, 32),
                'required': True,
                'formatter': 'N',
            },
            'company_agreement_number': {
                'code': '07.0',
                'size': 20,
                'description': 'Código do convênio no banco',
                'position': (33, 52),
                'required': True,
                'formatter': 'A',
            },
            'agency': {
                'code': '08.0',
                'size': 5,
                'description': 'Agência mantenedora da conta',
                'position': (53, 57),
                'required': True,
                'formatter': 'N',
            },
            'agency_digit': {
                'code': '09.0',
                'size': 1,
                'description': 'Dígito verificador da agência',
                'position': (58, 58),
                'required': True,
                'formatter': 'A',
            },
            'account_number': {
                'code': '10.0',
                'size': 12,
                'description': 'Número da conta corrente',
                'position': (59, 70),
                'required': True,
                'formatter': 'N',
            },
            'account_digit': {
                'code': '11.0',
                'size': 1,
                'description': 'Dígito verificador da conta',
                'position': (71, 71),
                'required': True,
                'formatter': 'A',
            },
            'digit_ag_account': {
                'code': '12.0',
                'size': 1,
                'description': 'Dígito verificador da ag/conta',
                'position': (72, 72),
                'required': False,
                'formatter': 'A',
                'default': ''
            },
            'company_name': {
                'code': '13.0',
                'size': 30,
                'description': 'Nome da empresa',
                'position': (73, 102),
                'required': False,
                'formatter': 'A',
            },
            'bank_name': {
                'code': '14.0',
                'size': 30,
                'description': 'Nome do banco',
                'position': (103, 132),
                'formatter': 'A',
                'required': False,
                'default': 'BANCO BRADESCO S.A'
            },
            'febraban_usage_cnab': {
                'code': '15.0',
                'size': 10,
                'description': 'Uso exclusivo da FEBRABAN',
                'position': (133, 142),
                'required': False,
                'formatter': 'A',
            },
            'shipping_return_code': {
                'code': '16.0',
                'size': 1,
                'description': 'Código de remessa/retorno',
                'position': (143, 143),
                'required': False,
                'formatter': 'N',
                'default': 1
            },
            'file_generation_date': {
                'code': '17.0',
                'size': 8,
                'description': 'Data da geração do arquivo',
                'position': (144, 151),
                'formatter': 'N',
                'required': False,
                'default': now().strftime('%d%m%Y')
            },
            'file_generation_time': {
                'code': '18.0',
                'size': 6,
                'description': 'Hora da geração do arquivo',
                'position': (152, 157),
                'required': False,
                'formatter': 'N',
                'default': now().strftime('%H%M%S')
            },
            'file_sequencial_number': {
                'code': '19.0',
                'size': 6,
                'description': 'Número sequencial do arquivo',
                'position': (158, 163),
                'required': True,
                'formatter': 'N',
            },
            'layout_number': {
                'code': '20.0',
                'size': 3,
                'description': 'Número da versão do layout do arquivo',
                'position': (164, 166),
                'formatter': 'N',
                'required': True,
                'default': '089'
            },
            'file_recording_density': {
                'code': '21.0',
                'size': 5,
                'description': 'Densidade de gravação do arquivo',
                'position': (167, 171),
                'required': False,
                'formatter': 'N',
                'default': 1600
            },
            'bank_reserved': {
                'code': '22.0',
                'size': 20,
                'description': 'Para uso reservado do banco',
                'position': (172, 191),
                'formatter': 'A',
                'required': False,
                'default': ''
            },
            'company_reserved': {
                'code': '23.0',
                'size': 20,
                'description': 'Para uso reservado da empresa',
                'position': (192, 211),
                'formatter': 'A',
                'required': False,
                'default': ''
            },
            'agreement_number': {
                'code': '24.0',
                'size': 22,
                'description': 'Código de convênio no banco',
                'position': (212, 233),
                'required': False,
                'formatter': 'N',
                'default': ''
            },
            'febraban_cnab_reserved': {
                'code': '24.0',
                'size': 7,
                'description': 'Uso exclusivo da FEBRABAN',
                'position': (234, 240),
                'required': False,
                'formatter': 'A',
                'default': ''
            }
        }
    },
    'trailer': {
        'fields': {
            'bank_code': {
                'code': '01.9',
                'size': 3,
                'description': 'Código do Banco na Compensação',
                'position': (1, 3),
                'required': False,
                'formatter': 'N',
                'default': 237
            },
            'batch': {
                'code': '02.9',
                'size': 4,
                'description': 'Lote de serviço',
                'position': (4, 7),
                'required': True,
                'formatter': 'N',
                'default': 9999
            },
            'record_type': {
                'code': '03.9',
                'size': 1,
                'description': 'Tipo de registro',
                'position': (8, 8),
                'required': True,
                'formatter': 'N',
                'default': 9
            },
            'febraban_usage': {
                'code': '04.9',
                'size': 9,
                'description': 'Uso exclusivo FEBRABAN/CNAB',
                'position': (9, 17),
                'required': False,
                'formatter': 'A',
                'default': ''
            },
            'batch_qty': {
                'code': '05.9',
                'size': 6,
                'description': 'Quantidade de lotes do arquivo',
                'position': (18, 23),
                'required': False,
                'formatter': 'N',
            },
            'file_records_qty': {
                'code': '06.9',
                'size': 6,
                'description': 'Quantidade de registros do arquivo',
                'position': (24, 29),
                'required': False,
                'formatter': 'N',
            },
            'accounts_qty': {
                'code': '07.9',
                'size': 6,
                'description': 'Quantidade de contas para conciliação',
                'position': (30, 35),
                'required': True,
                'formatter': 'N',
                'default': '0'
            },
            'febraban_usage_cnab': {
                'code': '08.9',
                'size': 205,
                'description': 'Uso exclusivo FEBRABAN/CNAB',
                'position': (36, 240),
                'required': False,
                'formatter': 'A',
                'default': ''
            },
        }
    },
    'batch': {
        'header': {
            'fields': {
                'bank_code': {
                    'size': 3,
                    'description': 'Código do Banco na Compensação',
                    'position': (1, 3),
                    'required': False,
                    'formatter': 'N',
                    'default': 237
                },
                'batch_number': {
                    'size': 4,
                    'description': 'Lote de serviço',
                    'position': (4, 7),
                    'formatter': 'N',
                },
                'record_type': {
                    'size': 1,
                    'description': 'Registro header de lote',
                    'position': (8, 8),
                    'formatter': 'N',
                    'default': 5
                },
                'operation_type': {
                    'size': 1,
                    'description': 'Tipo da operação',
                    'position': (9, 9),
                    'formatter': 'A',
                    'default': 'E'
                },
                'service_type': {
                    'size': 2,
                    'description': 'Tipo de serviço',
                    'position': (10, 11),
                    'formatter': 'N',
                    'default': '01'
                },
                'entry_type': {
                    'code': '06.1',
                    'size': 2,
                    'description': 'Forma de lançamento',
                    'position': (12, 13),
                    'formatter': 'N',
                    'default': '40'
                },
                'batch_layout': {
                    'size': 3,
                    'description': 'Nº da versão do layout do lote',
                    'position': (14, 16),
                    'formatter': 'N',
                    'default': '050'
                },
                'febraban_cnab_reserved': {
                    'size': 1,
                    'description': 'Uso exclusivo da FEBRABAN/CNAB',
                    'position': (17, 17),
                    'formatter': 'A',
                    'default': ''
                },
                'company_inscription_type': {
                    'size': 2,
                    'description': 'Tipo de inscrição da empresa',
                    'position': (18, 18),
                    'formatter': 'N'
                },
                'company_inscription_number': {
                    'size': 14,
                    'description': 'Nº de inscrição da empresa',
                    'position': (19, 32),
                    'formatter': 'N',
                },
                'company_agreement_number': {
                    'size': 20,
                    'description': 'Código do convênio no banco',
                    'position': (33, 52),
                    'formatter': 'A',
                },
                'company_agency': {
                    'size': 5,
                    'description': 'Agência mantenedora da conta',
                    'position': (53, 57),
                    'formatter': 'N',
                },
                'company_agency_digit': {
                    'size': 1,
                    'description': 'Dígito verificador da agência',
                    'position': (58, 58),
                    'formatter': 'A',
                },
                'company_account_number': {
                    'size': 12,
                    'description': 'Número da conta corrente',
                    'position': (59, 70),
                    'formatter': 'N',
                },
                'company_account_digit': {
                    'size': 1,
                    'description': 'Dígito verificador da conta',
                    'position': (71, 71),
                    'formatter': 'A',
                },
                'company_digit_ag_account': {
                    'size': 1,
                    'description': 'Dígito verificador da ag/conta',
                    'position': (72, 72),
                    'formatter': 'A',
                    'default': ''
                },
                'company_name': {
                    'size': 30,
                    'position': (73, 102),
                    'formatter': 'A',
                    'description': 'Nome da empresa'
                },
                'statement_second_line': {
                    'size': 40,
                    'position': (103, 142),
                    'formatter': 'A',
                    'description': 'Segunda linha do extrato de conta corrente'
                },
                'initial_balance_date': {
                    'size': 8,
                    'position': (143, 150),
                    'formatter': 'N',
                    'description': 'Data do saldo inicial',
                },
                'initial_balance_amount': {
                    'size': 16,
                    'position': (151, 168),
                    'formatter': 'N',
                    'description': 'Valor do saldo inicial',
                    'decimal_places': 2,
                },
                'initial_balance_situation': {
                    'size': 1,
                    'position': (169, 169),
                    'formatter': 'A',
                    'description': 'Situação do saldo inicial (D/C)'
                },
                'initial_balance_position': {
                    'size': 1,
                    'position': (170, 170),
                    'formatter': 'A',
                    'description': 'Posição do saldo inicial'
                },
                'statement_currency': {
                    'size': 3,
                    'position': (171, 173),
                    'formatter': 'A',
                    'description': 'Moeda referenciada no extrato'
                },
                'statement_sequency_number': {
                    'size': 5,
                    'position': (174, 178),
                    'formatter': 'N',
                    'description': 'Nº sequência do extrato'
                },
                'febraban_cnab_reserved_2': {
                    'size': 62,
                    'description': 'Uso exclusivo da FEBRABAN/CNAB',
                    'position': (179, 240),
                    'formatter': 'A',
                    'default': ''
                },
            }
        },
        'trailer': {
            'fields': {
                'bank_code': {
                    'size': 3,
                    'description': 'Código do Banco na Compensação',
                    'position': (1, 3),
                    'required': False,
                    'formatter': 'N',
                    'default': 237
                },
                'batch_number': {
                    'size': 4,
                    'description': 'Lote de serviço',
                    'position': (4, 7),
                    'formatter': 'N',
                },
                'record_type': {
                    'size': 1,
                    'description': 'Registro header de lote',
                    'position': (8, 8),
                    'formatter': 'N',
                    'default': 5
                },
                'febraban_cnab_reserved': {
                    'size': 9,
                    'description': 'Uso exclusivo da FEBRABAN/CNAB',
                    'position': (9, 17),
                    'formatter': 'A',
                    'default': ''
                },
                'company_inscription_type': {
                    'size': 2,
                    'description': 'Tipo de inscrição da empresa',
                    'position': (18, 18),
                    'formatter': 'N'
                },
                'company_inscription_number': {
                    'size': 14,
                    'description': 'Nº de inscrição da empresa',
                    'position': (19, 32),
                    'formatter': 'N',
                },
                'company_agreement_number': {
                    'size': 20,
                    'description': 'Código do convênio no banco',
                    'position': (33, 52),
                    'formatter': 'A',
                },
                'company_agency': {
                    'size': 5,
                    'description': 'Agência mantenedora da conta',
                    'position': (53, 57),
                    'formatter': 'N',
                },
                'company_agency_digit': {
                    'size': 1,
                    'description': 'Dígito verificador da agência',
                    'position': (58, 58),
                    'formatter': 'A',
                },
                'company_account_number': {
                    'size': 12,
                    'description': 'Número da conta corrente',
                    'position': (59, 70),
                    'formatter': 'N',
                },
                'company_account_digit': {
                    'size': 1,
                    'description': 'Dígito verificador da conta',
                    'position': (71, 71),
                    'formatter': 'A',
                },
                'company_digit_ag_account': {
                    'size': 1,
                    'description': 'Dígito verificador da ag/conta',
                    'position': (72, 72),
                    'formatter': 'A',
                    'default': ''
                },
                'febraban_cnab_reserved_2': {
                    'size': 16,
                    'description': 'Uso exclusivo da FEBRABAN/CNAB',
                    'position': (73, 88),
                    'formatter': 'A',
                    'default': ''
                },
                'linked_previous_day': {
                    'size': 16,
                    'position': (89, 106),
                    'formatter': 'N',
                    'description': 'Vinculado do dia anterior'
                },
                'account_limit': {
                    'size': 16,
                    'position': (107, 124),
                    'formatter': 'N',
                    'description': 'Limite da conta',
                    'decimal_places': 2,
                },
                'locked_balance': {
                    'size': 16,
                    'position': (125, 142),
                    'formatter': 'N',
                    'description': 'Saldo bloqueado',
                    'decimal_places': 2,
                },
                'final_balance_date': {
                    'size': 8,
                    'position': (143, 150),
                    'formatter': 'N',
                    'description': 'Data do saldo final',
                },
                'final_balance_amount': {
                    'size': 8,
                    'position': (151, 168),
                    'formatter': 'N',
                    'description': 'Valor do saldo final',
                    'decimal_places': 2,
                },
                'final_balance_situation': {
                    'size': 1,
                    'position': (169, 169),
                    'formatter': 'A',
                    'description': 'Situação do saldo final'
                },
                'final_balance_position': {
                    'size': 1,
                    'position': (170, 170),
                    'formatter': 'A',
                    'description': 'Posição do saldo final'
                },
                'batch_qty': {
                    'size': 6,
                    'description': 'Quantidade de registros do lote',
                    'position': (171, 176),
                    'formatter': 'N',
                },
                'debit_amount_sum': {
                    'size': 16,
                    'position': (177, 194),
                    'formatter': 'N',
                    'description': 'Somatória dos valores a débito',
                    'decimal_places': 2,
                },
                'credit_amount_sum': {
                    'size': 16,
                    'position': (195, 212),
                    'formatter': 'N',
                    'description': 'Somatória dos valores a crédito',
                    'decimal_places': 2,
                },
                'febraban_cnab_reserved_3': {
                    'size': 28,
                    'description': 'Uso exclusivo da FEBRABAN/CNAB',
                    'position': (213, 240),
                    'formatter': 'A',
                }
            },
        },
    },
    'statements': {
        'segment_e': {
            'bank_code': {
                'size': 3,
                'description': 'Código do banco na Compensação',
                'position': (1, 3),
                'formatter': 'N',
                'default': 237
            },
            'batch_number': {
                'size': 4,
                'description': 'Lote de serviço',
                'position': (4, 7),
                'formatter': 'N',
            },
            'record_type': {
                'code': '03.3A',
                'size': 1,
                'description': 'Tipo de registro',
                'position': (8, 8),
                'formatter': 'N',
                'default': 3
            },
            'record_number': {
                'size': 5,
                'description': 'Nº sequencial do registro no lote',
                'position': (9, 13),
                'formatter': 'N',
            },
            'segment_code': {
                'size': 1,
                'description': 'Código do segmento do reg. detalhe',
                'position': (14, 14),
                'formatter': 'A',
                'default': 'E'
            },
            'entry_type_real_simulated': {
                'size': 1,
                'position': (15, 15),
                'formatter': 'N',
                'description': 'Tipo de lançamento "O" REAL, "I" SIMULADO'
            },
            'febraban_cnab_reserved': {
                'size': 2,
                'description': 'Uso exclusivo da FEBRABAN/CNAB',
                'position': (16, 17),
                'formatter': 'A',
                'default': ''
            },
            'company_document_type': {
                'size': 1,
                'description': 'Tipo de inscrição da empresa',
                'position': (18, 18),
                'formatter': 'N'
            },
            'company_document_number': {
                'size': 14,
                'description': 'Número de inscrição da empresa',
                'position': (19, 32),
                'formatter': 'N'
            },
            'company_agreement_number': {
                'size': 20,
                'description': 'Código do convênio no banco',
                'position': (33, 52),
                'formatter': 'A',
            },
            'company_agency': {
                'size': 5,
                'description': 'Agência mantenedora da conta',
                'position': (53, 57),
                'formatter': 'N',
            },
            'company_agency_digit': {
                'size': 1,
                'description': 'Dígito verificador da agência',
                'position': (58, 58),
                'formatter': 'A',
            },
            'company_account_number': {
                'size': 12,
                'description': 'Número da conta corrente',
                'position': (59, 70),
                'formatter': 'N',
            },
            'company_account_digit': {
                'size': 1,
                'description': 'Dígito verificador da conta',
                'position': (71, 71),
                'formatter': 'A',
            },
            'company_digit_ag_account': {
                'size': 1,
                'description': 'Dígito verificador da ag/conta',
                'position': (72, 72),
                'formatter': 'A',
                'default': ''
            },
            'company_name': {
                'size': 30,
                'position': (73, 102),
                'formatter': 'A',
                'description': 'Nome da empresa'
            },
            'febraban_cnab_reserved_2': {
                'size': 6,
                'description': 'Uso exclusivo da FEBRABAN/CNAB',
                'position': (103, 108),
                'formatter': 'A',
                'default': ''
            },
            'entry_nature': {
                'size': 3,
                'position': (109, 111),
                'formatter': 'A',
                'description': 'Natureza do lançamento'
            },
            'entry_type_complement': {
                'size': 3,
                'position': (112, 113),
                'formatter': 'N',
                'description': 'Tipo de complemento lançamento'
            },
            'entry_complement': {
                'size': 20,
                'position': (114, 133),
                'formatter': 'A',
                'description': 'Complemento do lançamento'
            },
            'cpmf_exemption_identification': {
                'size': 1,
                'position': (134, 134),
                'formatter': 'A',
                'description': 'Identificação de isenção do CPMF'
            },
            'accounting_date': {
                'size': 8,
                'position': (135, 142),
                'formatter': 'N',
                'description': 'Data contábil',
            },
            'entry_date': {
                'size': 8,
                'position': (143, 150),
                'formatter': 'N',
                'description': 'Data do lançamento',
            },
            'entry_amount': {
                'size': 16,
                'decimal_places': 2,
                'position': (151, 168),
                'formatter': 'N',
                'description': 'Valor do lançamento'
            },
            'entry_type_debit_credit': {
                'size': 1,
                'position': (169, 169),
                'formatter': 'A',
                'description': 'Tipo do lançamento: valor a débito/crédito'
            },
            'entry_category': {
                'size': 3,
                'position': (170, 172),
                'formatter': 'N',
                'description': 'Categoria do lançamento'
            },
            'entry_bank_code': {
                'size': 4,
                'position': (173, 176),
                'formatter': 'A',
                'description': 'Código do lançamento no banco'
            },
            'entry_historical_description': {
                'size': 25,
                'position': (177, 201),
                'formatter': 'A',
                'description': 'Descrição do hsitórico do lançamento no banco'
            },
            'entry_evidentiary_document_number': {
                'size': 7,
                'position': (202, 208),
                'formatter': 'A',
                'description': 'Nº documento comprobatório do lançamento'
            },
            'entry_statement_second_line': {
                'size': 32,
                'position': (209, 240),
                'formatter': 'A',
                'description': '2º linha do extrato de conta corrente'
            }
        }
    },
}


def get_batches(*args, **kwargs):
    return common.get_batches(SPEC, *args, **kwargs)
