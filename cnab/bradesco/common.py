
from ..helpers import get_value


def get_batches(SPEC, batches_lines, batch_layout_number):
    batch_layout_type = None
    main_batches_list, batches_list = [], []
    main_batches_segments = ["segment_a", "segment_e", "segment_j"]
    batches_segments = ["segment_a", "segment_b", "segment_j", "segment_j52", "segment_z", "segment_e"]

    batch_layout_name = {
        "040": "bankslip",
        "045": "transfers",
        "050": "statements",
    }[batch_layout_number]
    batch_layout_type = SPEC[batch_layout_name]

    for line in batches_lines:
        batch_segment = line[13:14].lower()
        batch_j_complement = line[17:19] if batch_layout_number == "040" and line[17:19] == "52" else ""
        segment_name = f"segment_{batch_segment}{batch_j_complement}"
        batch_layout = batch_layout_type[segment_name]

        if segment_name not in batches_segments:
            continue

        batch = {
            k: get_value(line, spec)
            for k, spec in batch_layout.items()
        }

        batches_list.append(batch)
        if segment_name in main_batches_segments:
            main_batches_list.append(batch)

    return main_batches_list, batches_list
