febraban_cnab_reserved = "uso_exclusivo_febraban"
febraban_cnab_reserved_2 = "uso_exclusivo_febraban"
febraban_cnab_reserved_3 = "uso_exclusivo_febraban"

# segmento e
entry_type_real_simulated = "tipo_lancamento_real_simulado"
company_document_type = "empresa_tipo_documento"
company_agreement_number = "empresa_codigo_convenio"  # Código do convênio no banco
company_account_digit = "empresa_digito_conta"  # Dígito verificador da conta
company_digit_ag_account = "empresa_digito_verificador_conta"  # Dígito verificador da ag/conta
company_name = "nome_empresa"  # Nome da empresa
entry_nature = "natureza_lancamento"  # Natureza do lançamento
entry_type_complement = "tipo_complemento_lancamento"  # Tipo de complemento lançamento
entry_complement = "complemento_lancamento"  # Complemento do lançamento
cpmf_exemption_identification = "id_isencao_cpmf"  # Identificação de isenção do CPMF
accounting_date = "data_contabil"  # Data contábil
entry_date = "data_lancamento"  # Data do lançamento
entry_amount = "valor_lancamento"  # Valor do lançamento
entry_type_debit_credit = "tipo_lancamento_debito_credito"  # Tipo do lançamento: valor a débito/crédito
entry_category = "categoria_lancamento"  # Categoria do lançamento
entry_bank_code = "codigo_lancamento_banco"  # Código do lançamento no banco
entry_historical_description = "descricao_historico_lancamento"  # Descrição do hsitórico do lançamento no banco
entry_evidentiary_document_number = "numero_documento_comprobatorio_lancamento"  # Nº documento comprobatório do lançamento  # NOQA: E501
entry_statement_second_line = "segunda_linha_extrato_cc"  # 2º linha do extrato de conta corrente
record_number = "numero_registro_lote"  # Nº sequencial do registro no lote
segment_code = "codigo_segmento"  # Código do segmento do reg. detalhe

# Header do arquivo
bank_code = "codigo_banco"  # código do banco na compensação
batch_number = "numero_lote"  # Lote de serviço
registry_type = "tipo_registro"  # tipo de registro
company_document_number = "numero_inscricao_empresa"  # Número de inscrição da empresa
agency = "agencia"  # Agência mantenedora da conta
agency_digit = "digito_agencia"  # Dígito verificador da agência
account_number = "numero_conta"  # Número da conta corrente
account_digit = "digito_conta"  # Dígito verificador da conta
digit_ag_account = "digito_verificador_conta"  # Dígito verificador da ag/conta
bank_name = "nome_banco"  # Nome do banco
shipping_return_code = "codigo_remessa_retorno"  # Código de remessa/retorno
file_generation_date = "data_geracao_arquivo"  # Data da geração do arquivo
file_generation_time = "hora_geracao_arquivo"  # Hora da geração do arquivo
file_sequencial_number = "numero_sequencial_arquivo"  # Número sequencial do arquivo
layout_number = "numero_versao_layout"  # Número da versão do layout do arquivo
file_recording_density = "densidade_gravacao_arquivo"  # Densidade de gravação do arquivo
bank_reserved = "reservado"  # Para uso reservado do banco
company_reserved = "reservado_empresa"  # Para uso reservado da empresa
agreement_number = "codigo_convenio"  # Código de convênio no banco

# Trailer do arquivo
batch = "lote_servico"  # Lote de serviço
record_type = "tipo_registro"  # Tipo de registro
batch_qty = "quantidade_lote_arquivo"  # Quantidade de lotes do arquivo
file_records_qty = "quantidade_registro_arquivo"  # Quantidade de registros do arquivo
accounts_qty = "quantidade_contas"  # Quantidade de contas para conciliação

# Header do lote
operation_type = "tipo_operacao"  # Tipo da operação
service_type = "tipo_servico"  # Tipo de serviço
entry_type = "forma_lancamento"  # Forma de lançamento
batch_layout = "numero_versao_layout_lote"  # Nº da versão do layout do lote
company_inscription_type = "tipo_inscricao_empresa"  # Tipo de inscrição da empresa
company_inscription_number = "numero_inscricao_empresa"  # Nº de inscrição da empresa
company_agency = "conta_agencia"  # Agência mantenedora da conta
company_agency_digit = "digito_conta_agencia"  # Dígito verificador da agência
company_account_number = "numero_conta"  # Número da conta corrente
statement_second_line = "segunda_linha_extrato"  # Segunda linha do extrato de conta corrente
initial_balance_date = "data_saldo_inicial"  # Data do saldo inicial
initial_balance_amount = "valor_saldo_inicial"  # Valor do saldo inicial
initial_balance_situation = "situacao_saldo_inicial"  # Situação do saldo inicial (D/C)
initial_balance_position = "posicao_saldo_inicial"  # Posição do saldo inicial
statement_currency = "moeda_extrato"  # Moeda referenciada no extrato
statement_sequency_number = "numero_sequencia_extrato"  # Nº sequência do extrato

# Trailer do lote
linked_previous_day = "vinculado_dia_anterior"  # Vinculado do dia anterior
account_limit = "limite_conta"  # Limite da conta
locked_balance = "saldo_bloqueado"  # Saldo bloqueado
final_balance_date = "data_saldo_final"  # Data do saldo final
final_balance_amount = "valor_saldo_final"  # Valor do saldo final
final_balance_situation = "situacao_saldo_final"  # Situação do saldo final
final_balance_position = "posicao_saldo_final"  # Posição do saldo final
debit_amount_sum = "soma_valores_debito"  # Somatória dos valores a débito
credit_amount_sum = "soma_valores_credito"  # Somatória dos valores a crédito
