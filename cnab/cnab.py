import copy
import importlib
import logging

from cnab.helpers import get_value

from .base import Bankslip, Batch, BatchHeader, BatchTrailer, FileHeader, FileTrailer, Segment, SegmentB, SegmentJ52, Transfer
from .settings import LAYOUTS

logger = logging.getLogger(__name__)


class CNAB:
    def __init__(self, header, trailer):
        self.header = header
        self.trailer = trailer
        self.batchs = []
        self.segments = []

    def export(self, buff):
        self._export_block(buff, self.header)

        if self.segments:
            for record in self.segments:
                self._export_block(buff, record)
        else:
            for batch in self.batchs:
                self._export_block(buff, batch.header)
                for payment in batch.payments:
                    self._export_block(buff, payment)
                    if hasattr(payment, 'detail'):
                        self._export_block(buff, payment.detail)

                self._export_block(buff, batch.trailer)

        self._export_block(buff, self.trailer)

        return buff.getvalue()

    def _export_block(self, buff, block):
        for field in block.fields:
            value = getattr(block, field)
            content = value.serialize()

            logger.debug(f'field {field} = {content}')
            buff.write(content)

        buff.write('\r\n')


class CnabFactory:

    @staticmethod
    def get_cnab(bank, layout, version):
        """
        Return a cnab file class configured with field in spec for bank, layout and version parameters.
        """
        config = LAYOUTS[bank]['layout'][layout]['version'][version]['spec']
        module = importlib.import_module(config)
        spec = module.SPEC

        header_fields = spec['header']['fields']
        file_header = FileHeader(fields=header_fields)

        trailer_fields = spec['trailer']['fields']
        file_trailer = FileTrailer(fields=trailer_fields)

        return CNAB(header=file_header, trailer=file_trailer)

    @staticmethod
    def get_batch(bank, layout, version):
        """
        Return a cnab batch class configured with field in spec for bank, layout and version parameters.
        """
        config = LAYOUTS[bank]['layout'][layout]['version'][version]['spec']
        module = importlib.import_module(config)
        spec = module.SPEC

        batch_fields = copy.deepcopy(spec['batch']['header']['fields'])
        header = BatchHeader(fields=batch_fields)

        trailer_fields = copy.deepcopy(spec['batch']['trailer']['fields'])
        trailer = BatchTrailer(fields=trailer_fields)

        return Batch(header=header, trailer=trailer)

    @staticmethod
    def get_transfer(bank, layout, version):
        """
        Return a transfer payment configured with field in spec for bank, layout and version parameters.
        """
        config = LAYOUTS[bank]['layout'][layout]['version'][version]['spec']
        module = importlib.import_module(config)
        spec = module.SPEC

        payment_fields = copy.deepcopy(spec['transfers']['segment_a'])
        transfer = Transfer(fields=payment_fields)
        return transfer

    @staticmethod
    def get_detail(bank, layout, version):
        """
        Return a cnab transfer detail (B Segment) configured with field in spec for bank, layout and version parameters.
        """
        config = LAYOUTS[bank]['layout'][layout]['version'][version]['spec']
        module = importlib.import_module(config)
        spec = module.SPEC

        detail_fields = copy.deepcopy(spec['transfers']['segment_b'])
        detail = SegmentB(fields=detail_fields)
        return detail

    @staticmethod
    def get_bankslip(bank, layout, version):
        """
        Return a Bankslip payment configured with field in spec for bank, layout and version parameters.
        """
        config = LAYOUTS[bank]['layout'][layout]['version'][version]['spec']
        module = importlib.import_module(config)
        spec = module.SPEC

        payment_fields = copy.deepcopy(spec['bankslip']['segment_j'])
        return Bankslip(fields=payment_fields)

    @staticmethod
    def get_bankslip_detail(bank, layout, version):
        """
        Return a Bankslip payment detail configured with field in spec for bank, layout and version parameters.
        """
        config = LAYOUTS[bank]['layout'][layout]['version'][version]['spec']
        module = importlib.import_module(config)
        spec = module.SPEC

        detail_fields = copy.deepcopy(spec['bankslip']['segment_j52'])
        return SegmentJ52(fields=detail_fields)

    @staticmethod
    def get_segment(bank, layout, version, type_name, name):
        """
        Returns a generic segment configured with field in spec for bank, layout and version parameters.
        """
        config = LAYOUTS[bank]['layout'][layout]['version'][version]['spec']
        module = importlib.import_module(config)
        spec = module.SPEC

        detail_fields = copy.deepcopy(spec[type_name][name])
        detail = Segment(fields=detail_fields)
        return detail


class CNABReader:
    def __init__(self):
        self.file_dict = None
        self.main_batches_list = []

    def read(self, bank, layout, version, file_lines_list):
        config = LAYOUTS[bank]["layout"][layout]["version"][version]["spec"]
        module = importlib.import_module(config)
        spec = module.SPEC

        if type(file_lines_list) is not list or len(file_lines_list) < 3:
            raise Exception("Arquivo inválido")

        # Remove linha em branco do final (se presente)
        if not file_lines_list[-1]:
            file_lines_list.pop(len(file_lines_list) - 1)

        header_file_line = file_lines_list.pop(0)
        trailer_file_line = file_lines_list.pop(len(file_lines_list) - 1)
        batches_lines = file_lines_list

        header_file = self.get_header_file(spec, header_file_line)
        trailer_file = self.get_trailer_file(spec, trailer_file_line)

        self.file_dict = {"header": header_file, "trailer": trailer_file, "errors": None}

        # Alguns specs não possuem lotes
        if spec["batch"]:
            self.main_batches_list, whole_batches = self.get_batches_list(spec, module, batches_lines)
            self.file_dict["batches"] = whole_batches
        else:
            self.file_dict["segments"] = module.get_segments(batches_lines)

    @staticmethod
    def get_header_file(spec, h):
        header_fields = spec["header"]["fields"]
        header = {
            k: get_value(h, spec)
            for k, spec in header_fields.items()
        }
        return header

    @staticmethod
    def get_trailer_file(spec, t):
        trailer_fields = spec["trailer"]["fields"]
        trailer = {
            k: get_value(t, spec)
            for k, spec in trailer_fields.items()
        }
        return trailer

    @staticmethod
    def get_batches_list(spec, module, batches_list):
        file_batches_list = []
        main_batches_list = []
        batch_qtd_positions = spec["batch"]["trailer"]["fields"]["batch_qty"]["position"]

        while len(batches_list) > 0:
            last_trailer = batches_list[-1]
            batch_size = int(last_trailer[batch_qtd_positions[0] - 1:batch_qtd_positions[1]].lstrip("0"))

            # Agibank: ajuste para batch_qty, que não inclui header e trailer
            if module.__package__.endswith("santander"):
                batch_size += 2

            batch_lines = batches_list[-batch_size:]
            del batches_list[-batch_size:]

            # header line
            h = batch_lines.pop(0)
            header_fields = spec["batch"]["header"]["fields"]
            header = {
                k: get_value(h, spec)
                for k, spec in header_fields.items()
            }

            # trailer line
            t = batch_lines.pop(-1)
            trailer_fields = spec["batch"]["trailer"]["fields"]
            trailer = {
                k: get_value(t, spec)
                for k, spec in trailer_fields.items()
            }

            # batch lines
            batch_layout_number = header["batch_layout"]
            main_batches, batches = module.get_batches(batch_lines, batch_layout_number)

            main_batches_list = main_batches_list + main_batches
            file_batches_list.append({"header": header, "trailer": trailer, "batch": batches})
        return main_batches_list, file_batches_list
