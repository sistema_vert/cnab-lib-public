[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sistema_vert_cnab-lib&metric=alert_status&token=2f8343e5a5815eada9790c6baf5e453055a2903a)](https://sonarcloud.io/dashboard?id=sistema_vert_cnab-lib)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=sistema_vert_cnab-lib&metric=code_smells&token=2f8343e5a5815eada9790c6baf5e453055a2903a)](https://sonarcloud.io/dashboard?id=sistema_vert_cnab-lib)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sistema_vert_cnab-lib&metric=coverage&token=2f8343e5a5815eada9790c6baf5e453055a2903a)](https://sonarcloud.io/dashboard?id=sistema_vert_cnab-lib)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=sistema_vert_cnab-lib&metric=vulnerabilities&token=2f8343e5a5815eada9790c6baf5e453055a2903a)](https://sonarcloud.io/dashboard?id=sistema_vert_cnab-lib)
# Vert CNAB

Essa lib tem por finalidade lidar com arquivos cnab(escrita e leitura)

A premissa para essa lib é conseguir implementar layouts bancários de maneira simples.
Todos os layouts homologados, poderão ser usados em qualquer aplicação.

## Criar ambiente de desenvolvimento
```
source .venv/bin/activate
pip install -U pip setuptools
pip install -r requirements-dev.txt
```
## Lendo um arquivo de retorno cnab
Recebemos uma lista de strings onde cada índice representa uma linha do arquivo CNAB e retornamos uma lista de dicionários.
```
cnab = CNABReader()
cnab.read("bradesco", "240", "04", file_lines_list)
```

## Retorno do header através do dict do arquivo completo (file_dict)
```
cnab.file_dict['header']
output: 
{
    'bank_code': '237',
    'batch_number': '0000',
    'registry_type': '0',
    'febraban_usage': '',
    'company_document_type': '2',
    'cnpj': '20067830000143',
    'agreegment_number': '301231',
    'agency': '01111',
    'agency_digit': '0',
    'account_number': '000000001111',
    'account_digit': '0',
    'digit_ag_account': '',
    'company_name': 'CNAB enterprise SA',
    'bank_name': 'BANCO BRADESCO S.A.',
    'febraban_usage_cnab': '',
    'shipping_return_code': '2',
    'file_generation_date': '16112021',
    'hour_generation_date': '165522',
    'file_sequencial_number': '000001',
    'layout_number': '089',
    'density': '00000',
    'bank_reserved': '00000000BRAD',
    'company_reserved': '',
    'febraban_cnab_reserved': ''
}
 ```

## Retorno do trailer através do dict do arquivo completo (file_dict)
```
 cnab.file_dict['trailer']
 output:
{
    'bank_code': '237',
    'batch': '9999',
    'record_type': '9',
    'febraban_usage': '',
    'batch_qty': '000005',
    'file_records_qty': '000033',
    'accounts_qty': '',
    'febraban_usage_cnab': ''
}
```

## Retorno da lista de pagamentos (linhe principal e até 2 complementos) através do dict do arquivo completo (file_dict)
```
[{
    'header': {
        'bank_code': '237',
        'batch_number': '0005',
        'record_type': '1',
        'operation_type': 'C',
        'service_type': '20',
        'launch_form': '30',
        'batch_layout': '040',
        'febraban_usage': '',
        'company_document_type': '2',
        'cnpj': '20067830000143',
        'agreegment_number': '301231',
        'agency': '01111',
        'agency_digit': '0',
        'account_number': '000000001111',
        'account_digit': '5',
        'digit_ag_account': '',
        'company_name': 'CNAB enterprise SA',
        'message': '1',
        'street': '',
        'street_number': '00000',
        'complement': '',
        'city': '',
        'zipcode': '00007',
        'zipcode_complement': '003',
        'state': 'SP',
        'payment_service': '',
        'febraban_usage_cnab': '',
        'ocurrency_code': ''
   },
    'trailer': {
        'bank_code': '237',
        'batch': '0005',
        'record_type': '5',
        'febraban_usage': '',
        'batch_qty': '000011',
        'amount_sum': '000000000002442657',
        'currency_qty_sum': '000000000000000000',
        'debit_notice': '000000',
        'febraban_usage_cnab': '',
        'ocurrency_code': ''
    },
    'batch': {
        'bank_code': '237',
        'batch_number': '0005',
        'record_type': '3',
        'record_number': '00001',
        'segment_code': 'J',
        'movement_type': '0',
        'instruction_code': '00',
        'barcode': '23792881200006409792373090000004281400047000',
        'assignor_name': 'CNAB enterprise SA',
        'due_date': '22112021',
        'title_amount': '000000000640979',
        'discount_amount': '000000000000000',
        'late_payment': '000000000000000',
        'payment_date': '16112021',
        'amount': '000000000640979',
        'currency_qty': '000000640900000',
        'payment_id': '8836',
        'payment_bank_id': '8836',
        'currency_code': '09',
        'febraban_usage_cnab': '',
        'ocurrency_code': '00'
    },
    'batch_complement': {
        'bank_code': '237',
        'batch_number': '0005',
        'record_type': '3',
        'record_number': '00002',
        'segment_code': 'J',
        'febraban_usage': '',
        'shipping_moving_code': '',
        'optional_registration_identification': '52',
        'drawee_document_type': '2',
        'drawee_document_number': '020067830000143',
        'drawee_name': 'CNAB enterprise SA',
        'assignor_document_type': '2',
        'assignor_document_number': '020067830000143',
        'assignor_name': 'CNAB enterprise SA',
        'drawer_document_type': '2',
        'drawer_document_number': '020067830000143',
        'drawer_name': 'CNAB enterprise SA',
        'febraban_usage_cnab': ''
    }
},]
```

## Retorno da lista da principal linha de pagamento em listagem direta (main_batches_list)
```
cnab.main_batches_list
output:
[{
    'bank_code': '237',
    'batch_number': '0005',
    'record_type': '3',
    'record_number': '00001',
    'segment_code': 'J',
    'movement_type': '0',
    'instruction_code': '00',
    'barcode': '23792881200006409792373090000004281400047000',
    'assignor_name': 'CNAB enterprise SA',
    'due_date': '22112021',
    'title_amount': '000000000640979',
    'discount_amount': '000000000000000',
    'late_payment': '000000000000000',
    'payment_date': '16112021',
    'amount': '000000000640979',
    'currency_qty': '000000640900000',
    'payment_id': '8836',
    'payment_bank_id': '8836',
    'currency_code': '09',
    'febraban_usage_cnab': '',
    'ocurrency_code': '00'
},]
```

## Criando um arquivo cnab
```
cnab = CnabFactory.get_cnab('bradesco', '240', '04')
headers = {
    'cnpj': '05422962000152',
    'agreegment_number': '98988',
    'agency': '3396',
    'agency_digit': '9',
    'account_number': '3951',
    'account_digit': '9',
    'company_name': 'VERT CAPITAL',
    'file_sequencial_number': 1,
    'company_reserved': '',
}

cnab.header.set_attributes(**headers)
```

## criando um lote
```
bacth = CnabFactory.get_batch('bradesco', '240', '04')

header= {
    'bank_code': 237,
    'batch_number': 1,
    'company_document_type': 2,  # CNPJ
    'cnpj': '05422962000152',
    'agreegment_number': '98988',
    'agency': '3396',
    'agency_digit': '0',
    'account_number': ACCOUNT,
    'account_digit': ACCOUNT_DIGIT,
    'company_name': 'PATRIMONIO DE TESTE',
    'message': '1',
    'ocurrency_code': '',
    'zipcode': '06083',
    'zipcode_complement': '160',
    'state': 'SP'
}
bacth.header.set_attributes(**head_transfer)

batch_trailer = {
    'bank_code': 237,
    'batch': 1,
    'batch_qty': 5,
    'amount_sum': Decimal('1080.11'),
    'currency_qty_sum': Decimal('0.0'),
}
bacth.trailer.set_attributes(**batch_trailer)
```

## criando uma TED
```
transfer_ted = CnabFactory.get_transfer('bradesco', '240', '04')
ted_data = {
    'bank_code': 237,
    'batch_number': 1,
    'record_number': 1,
    'movement_type': 0,  # inclusao
    'instrution_code': '00',  # Inclusao de registro de detalhe liberado
    'chamber_code': '018',  # 018 TED 700 DOC
    'grantee_bank_code': 341,
    'grantee_agency': 2343,
    'grantee_agency_digit': 3,
    'grantee_account_number': 23422,
    'grantee_account_digit': 2,
    'grantee_name': 'JOTAGE SALES',
    'payment_id': 1212412,
    'payment_date': NOW,
    'currency_qty': Decimal('1080.11'),
    'amount': Decimal('1080.11'),
    'payment_real_date': NOW,
    'real_amount': Decimal('1080.11'),
    'doc_purpose_code': ' ',
    'ted_purpose_code': '10',  # pagamento a fornecedores
    'complement_purpose_code': 'CC',  # CC conta corrente e PP conta poupança
    'grantee_notice': '0',  # nao emite aviso
}

transfer_ted.set_attributes(**ted_data)

transfer_ted_detail = CnabFactory.get_detail('bradesco', '240', '04')

grantee_detail_data = {
    'bank_code': 237,
    'batch_number': 1,
    'record_number': 1,
    'cnpj': '91038859000145',
    'amount': Decimal('1080.11'),
    'rebate_amount': Decimal('0.00'),
    'discount': Decimal('0.00'),
    'mora_amount': Decimal('0.00'),
    'fine_amount': Decimal('0.00'),
}
transfer_ted_detail.set_attributes(**grantee_detail_data)

transfer_ted.add_detail(transfer_ted_detail)
```

## Criando uma DOC
```
transfer_doc = CnabFactory.get_transfer('bradesco', '240', '04')

doc_data = {
    'bank_code': 237,
    'batch_number': 1,
    'record_number': 2,
    'movement_type': 0,  # inclusao
    'instrution_code': '00',  # Inclusao de registro de detalhe liberado
    'chamber_code': '700',  # 018 TED 700 DOC
    'grantee_bank_code': 341,
    'grantee_agency': 2343,
    'grantee_agency_digit': 3,
    'grantee_account_number': 23422,
    'grantee_account_digit': 2,
    'grantee_name': 'JOTAGE SALES DOC',
    'payment_id': 87676,
    'payment_date': NOW,
    'currency_qty': Decimal('997.11'),
    'amount': Decimal('997.11'),
    'payment_real_date': NOW,
    'real_amount': Decimal('997.11'),
    'doc_purpose_code': '07',  # pagamento a fornecedores
    'ted_purpose_code': '     ',
    'complement_purpose_code': 'CC',  # CC conta corrente e PP conta poupança
    'grantee_notice': '0',  # nao emite aviso
}

transfer_doc.set_attributes(**doc_data)

grantee_doc_data = {
    'bank_code': 237,
    'batch_number': 1,
    'record_number': 1,
    'cnpj': '91038859000145',
    'amount': Decimal('997.11'),
    'rebate_amount': Decimal('0.00'),
    'discount': Decimal('0.00'),
    'mora_amount': Decimal('0.00'),
    'fine_amount': Decimal('0.00'),
}

transfer_doc_detail = CnabFactory.get_detail('bradesco', '240', '04')
transfer_doc_detail.set_attributes(**grantee_doc_data)

transfer_doc.add_detail(transfer_doc_detail)
```

## Adicionando um pagamento a um lote
```
bacth.add_payment(transfer_doc)
```

## Pagamento com boleto
```
bankslip_data = {
    'bank_code': 237,
    'batch_number': 1,
    'record_number': 3,
    'movement_type': 0,  # inclusao
    'barcode': '93381286002668558057000063305282520000108011',
    'assignor_name': 'JOTAGE SALES BOLETO',
    'due_date': '05112020',
    'title_amount': Decimal('1080.11'),
    'discount_amount': Decimal('0.0'),
    'late_payment': Decimal('0.0'),
    'payment_date': NOW,
    'amount': Decimal('1080.11'),
    'currency_qty': Decimal('1080.11'),
    'payment_id': 345345,
    'ocurrency_code': ' '
}
bankslip = CnabFactory.get_bankslip('bradesco', '240', '04')
bankslip.set_attributes(**bankslip_data)

bacth.add_payment(bankslip)
```

## Adicionando um lote de pagamentos ao arquivo CNAB
```
cnab.batchs.append(bacth)
```

## Trailer do cnab
```
trailer = {
    'bank_code': 237,
    'batch_qty': 1,
    'file_records_qty': 9,
}
cnab.trailer.set_attributes(**trailer)

```
## Exportando um arquivo CNAB
```
# criando um string buffer para armazenar o conteúdo do CNAB
buffer = StringIO()
content = cnab.export(buffer)
```
