clean:
	@echo "Execute cleaning ..."
	rm -f *.pyc
	rm -f .coverage
	rm -f application/coverage.xml

pep8:
	flake8 .
	isort . --check-only

test: clean pep8
	pytest --cov=.

fix-import: clean
	isort . -rc
