
from decimal import Decimal
from io import StringIO

from cnab import CnabFactory
from cnab.helpers import now

NOW = now().strftime('%d%m%Y')
CNPJ = '05422962000152'
ACCOUNT = '3951'
ACCOUNT_DIGIT = 9
AG = 3396
AG_DIGIT = '0'
BANCK_CODE = 237
AGREEGMENT_NUMBER = 98988

cnab = CnabFactory.get_cnab('bradesco', '240', '04')

headers = {
    'cnpj': CNPJ,
    'agreegment_number': AGREEGMENT_NUMBER,
    'agency': AG,
    'agency_digit': AG_DIGIT,
    'account_number': ACCOUNT,
    'account_digit': ACCOUNT_DIGIT,
    'company_name': 'VERT CAPITAL',
    'file_sequencial_number': 1,
    'company_reserved': '',
}

cnab.header.set_attributes(**headers)

bacth = CnabFactory.get_batch('bradesco', '240', '04')
head_transfer = {
    'batch_number': 1,
    'company_document_type': 2,  # CNPJ
    'launch_form': '03',
    'cnpj': CNPJ,
    'agreegment_number': AGREEGMENT_NUMBER,
    'agency': AG,
    'agency_digit': '0',
    'account_number': ACCOUNT,
    'account_digit': ACCOUNT_DIGIT,
    'company_name': 'PATRIMONIO DE TESTE',
    'message': '1',
    'ocurrency_code': '',
    'zipcode': '06083',
    'zipcode_complement': '160',
    'state': 'SP'
}

bacth.header.set_attributes(**head_transfer)


trailer_transfers = {
    'batch': 1,
    'batch_qty': 5,
    'amount_sum': Decimal('1080.11'),
    'currency_qty_sum': Decimal('0.0'),  # atualizar lib para chumbar 0.0
}
bacth.trailer.set_attributes(**trailer_transfers)

# ##########################################################################
# TED
# ##########################################################################

transfer_ted = CnabFactory.get_transfer('bradesco', '240', '04')

ted_data = {
    'batch_number': 1,
    'record_number': 1,
    'movement_type': 0,  # inclusao
    'instrution_code': '00',  # Inclusao de registro de detalhe liberado
    'chamber_code': '018',  # 018 TED 700 DOC
    'grantee_bank_code': 341,
    'grantee_agency': 2343,
    'grantee_agency_digit': 3,
    'grantee_account_number': 23422,
    'grantee_account_digit': 2,
    'grantee_name': 'JOTAGE SALES',
    'payment_id': 1212412,
    'payment_date': NOW,
    'currency_qty': Decimal('1080.11'),
    'amount': Decimal('1080.11'),
    'payment_real_date': NOW,
    'real_amount': Decimal('1080.11'),
    'doc_purpose_code': '  ',
    'ted_purpose_code': '10',  # pagamento a fornecedores
    'complement_purpose_code': 'CC',  # CC conta corrente e PP conta poupança
    'grantee_notice': '0',  # nao emite aviso
}

transfer_ted.set_attributes(**ted_data)

transfer_ted_detail = CnabFactory.get_detail('bradesco', '240', '04')

grantee_detail_data = {
    'bank_code': 237,
    'batch_number': 1,
    'record_number': 1,
    'cnpj': '91038859000145',
    'amount': Decimal('1080.11'),
    'rebate_amount': Decimal('0.00'),
    'discount': Decimal('0.00'),
    'mora_amount': Decimal('0.00'),
    'fine_amount': Decimal('0.00'),
    'due_date': '05112020',
}
transfer_ted_detail.set_attributes(**grantee_detail_data)

transfer_ted.add_detail(transfer_ted_detail)

bacth.add_payment(transfer_ted)


# ##########################################################################
# DOC
# ##########################################################################
transfer_doc = CnabFactory.get_transfer('bradesco', '240', '04')

doc_data = {
    'batch_number': 1,
    'record_number': 2,
    'movement_type': 0,  # inclusao
    'instrution_code': '00',  # Inclusao de registro de detalhe liberado
    'chamber_code': '700',  # 018 TED 700 DOC
    'grantee_bank_code': 341,
    'grantee_agency': 2343,
    'grantee_agency_digit': 3,
    'grantee_account_number': 23422,
    'grantee_account_digit': 2,
    'grantee_name': 'JOTAGE SALES DOC',
    'payment_id': 87676,
    'payment_date': NOW,
    'currency_qty': Decimal('997.11'),
    'amount': Decimal('997.11'),
    'payment_real_date': NOW,
    'real_amount': Decimal('997.11'),
    'doc_purpose_code': '1',  # pagamento a fornecedores
    'ted_purpose_code': '     ',
    'complement_purpose_code': 'CC',  # CC conta corrente e PP conta poupança
    'grantee_notice': '0',  # nao emite aviso
}

transfer_doc.set_attributes(**doc_data)

grantee_doc_data = {
    'batch_number': 1,
    'record_number': 1,
    'cnpj': '91038859000145',
    'amount': Decimal('997.11'),
    'rebate_amount': Decimal('0.00'),
    'discount': Decimal('0.00'),
    'mora_amount': Decimal('0.00'),
    'fine_amount': Decimal('0.00'),
    'due_date': '05112020',
}

transfer_doc_detail = CnabFactory.get_detail('bradesco', '240', '04')
transfer_doc_detail.set_attributes(**grantee_doc_data)

transfer_doc.add_detail(transfer_doc_detail)
bacth.add_payment(transfer_doc)

# ##########################################################################
# BANKSLIP
# ##########################################################################
bankslip_data = {
    'batch_number': 1,
    'record_number': 3,
    'movement_type': 0,  # inclusao
    'barcode': '93381286002668558057000063305282520000108011',
    'assignor_name': 'JOTAGE SALES BOLETO',
    'due_date': '05112020',
    'title_amount': Decimal('1080.11'),
    'discount_amount': Decimal('0.0'),
    'late_payment': Decimal('0.0'),
    'payment_date': NOW,
    'amount': Decimal('1080.11'),
    'currency_qty': Decimal('1080.11'),
    'payment_id': 345345,
    'ocurrency_code': ' '
}
bankslip = CnabFactory.get_bankslip('bradesco', '240', '04')
bankslip.set_attributes(**bankslip_data)

bankslip_detail_data = {
    'batch_number': 1,
    'record_type': 1,
    'record_number': 2,
    'segment_code': "0",
    'shipping_moving_code': 1,
    'optional_registration_identification': 12,
    'drawee_document_type': 1,
    'drawee_document_number': 123456,
    'drawee_name': 'JOTAGE SALES BOLETO',
    'assignor_document_type': 1,
    'assignor_document_number': 123456,
    'assignor_name': 'JOTAGE SALES BOLETO',
    'drawer_document_type': 1,
    'drawer_document_number': 123456,
    'drawer_name': 'JOTAGE SALES BOLETO',
}

bankslip_detail = CnabFactory.get_bankslip_detail('bradesco', '240', '04')
bankslip_detail.set_attributes(**bankslip_detail_data)

bankslip.add_detail(bankslip_detail)

bacth.add_payment(bankslip)

# ##########################################################################
# SAME BANK
# ##########################################################################
batch_same_bank = CnabFactory.get_batch('bradesco', '240', '04')
head_transfer['company_name'] = 'TRANSFER SAME BANK'
head_transfer['batch_number'] = 2
head_transfer['launch_form'] = '01'  # credito em conta

batch_same_bank.header.set_attributes(**head_transfer)

trailer_same_bank = {
    'batch': 2,
    'batch_qty': 4,
    'amount_sum': Decimal('1080.11'),
    'currency_qty_sum': Decimal('0.0'),  # atualizar lib para chumbar 0.0
}
batch_same_bank.trailer.set_attributes(**trailer_same_bank)

transfer_same_bank = CnabFactory.get_transfer('bradesco', '240', '04')

same_bank_data = {
    'batch_number': 1,
    'record_number': 1,
    'movement_type': 0,  # inclusao
    'instrution_code': '00',  # Inclusao de registro de detalhe liberado
    'chamber_code': '000',  # 018 TED 700 DOC 000 transfer same bank
    'grantee_bank_code': 237,
    'grantee_agency': 3049,
    'grantee_agency_digit': '',
    'grantee_account_number': 706729,
    'grantee_account_digit': 1,
    'grantee_name': 'JOTAGE SALES SAME BANK',
    'payment_id': 123456,
    'payment_date': NOW,
    'currency_qty': Decimal('1080.11'),
    'amount': Decimal('1080.11'),
    'payment_real_date': NOW,
    'real_amount': Decimal('1080.11'),
    'doc_purpose_code': ' ',
    'ted_purpose_code': '',  # pagamento a fornecedores
    'complement_purpose_code': 'CC',  # CC conta corrente e PP conta poupança
    'grantee_notice': '0',  # nao emite aviso
}

transfer_same_bank.set_attributes(**same_bank_data)

same_bank_detail = CnabFactory.get_detail('bradesco', '240', '04')

same_bank_detail_data = {
    'batch_number': 1,
    'record_number': 2,
    'cnpj': '91038859000145',
    'amount': Decimal('1080.11'),
    'rebate_amount': Decimal('0.00'),
    'discount': Decimal('0.00'),
    'mora_amount': Decimal('0.00'),
    'fine_amount': Decimal('0.00'),
    'due_date': '05112020',
}
same_bank_detail.set_attributes(**same_bank_detail_data)

transfer_same_bank.add_detail(same_bank_detail)

batch_same_bank.add_payment(transfer_same_bank)

# ##########################################################################
# ADD BATCH TO CNAB
# ##########################################################################
cnab.batchs.append(batch_same_bank)
cnab.batchs.append(bacth)

trailer = {
    'bank_code': 237,
    'batch_qty': 2,
    'file_records_qty': 13,
}
cnab.trailer.set_attributes(**trailer)

buff = StringIO()
content = cnab.export(buff)
print(content)

with open('examples/cnab_payments.txt', 'w') as f:
    f.write(content)
