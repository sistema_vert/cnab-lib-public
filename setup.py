import os

from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='vertc-cnab',
    version='0.0.3',
    packages=find_packages(),
    include_package_data=True,
    license='BSD License',
    description='cnab generation',
    long_description=README,
    url='https://vert-capital.com/',
    author='Vert Capital',
    author_email='produtosdigitais@vert-capital.com',
    classifiers=[],
    install_requires=[
        'pytz'
    ]
)
